package cn.slera.blogs.common.exception.handler;

import cn.slera.blogs.common.exception.CustomException;
import cn.slera.blogs.common.response.ResultCode;
import cn.slera.blogs.common.response.Result;
import cn.slera.blogs.entity.ExceptionLogs;
import cn.slera.blogs.service.ExceptionLogsService;
import cn.slera.blogs.util.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.PoolException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.ConnectException;
import java.sql.DataTruncation;
import java.sql.SQLDataException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 全局异常处理器
 * @author pyy
 */
@RestControllerAdvice
public class GlobalExceptionHandler{

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    @Autowired
    private ExceptionLogsService exceptionLogsService;

    /**
     * 处理自定义异常
     */
	@ExceptionHandler(CustomException.class)
	public Result handleException(CustomException e) {
        // 打印异常信息
        log.error("### 异常信息:{} ###", e.getMessage());

        // 返回结果
        Result  result =  new Result(e.getResultCode());

        //存入到数据库中
        ExceptionLogs logsDate = this.createExceptionLogs(e,result);
        exceptionLogsService.addExceptionLogs(logsDate);

        return result;
	}

    /**
     * 参数错误异常
     */
    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    public Result handleException(Exception e) {

        if (e instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException validException = (MethodArgumentNotValidException) e;
            BindingResult result = validException.getBindingResult();
            StringBuffer errorMsg = new StringBuffer();
            if (result.hasErrors()) {
                List<ObjectError> errors = result.getAllErrors();
                errors.forEach(p ->{
                    FieldError fieldError = (FieldError) p;
                    errorMsg.append(fieldError.getDefaultMessage()).append(",");
                    log.error("### 请求参数错误：{"+fieldError.getObjectName()+"},field{"+fieldError.getField()+ "},errorMessage{"+fieldError.getDefaultMessage()+"}"); });
            }
        } else if (e instanceof BindException) {
            BindException bindException = (BindException)e;
            if (bindException.hasErrors()) {
                log.error("### 请求参数错误: {}", bindException.getAllErrors());
            }
        }

        // 返回结果
        Result  result =  new Result(ResultCode.PARAM_IS_INVALID);

        //存入到数据库中
        ExceptionLogs logsDate = this.createExceptionLogs(e,result);
        exceptionLogsService.addExceptionLogs(logsDate);

        return result;
    }
    /**
     * 参数错误异常
     */
    @ExceptionHandler({BadCredentialsException.class})
    public Result BadCredentialsException(BadCredentialsException e) {
        //打印异常堆栈信息
        e.printStackTrace();
        // 打印异常信息
        log.error("### 登录异常:{} ###", e.getMessage());

        // 返回结果
        Result  result =  new Result(ResultCode.USER_LOGIN_ERROR);

        //存入到数据库中
        ExceptionLogs logsDate = this.createExceptionLogs(e,result);
        exceptionLogsService.addExceptionLogs(logsDate);

        return result;
    }


    /**
     * 下标越界异常
     */
    @ExceptionHandler(IndexOutOfBoundsException.class)
    public Result IndexOutOfBoundsException(IndexOutOfBoundsException e){
        //打印异常堆栈信息
        e.printStackTrace();
        // 打印异常信息
        log.error("### 下标越界异常异常:{} ###", e.getMessage());

        // 返回结果
        Result  result =  new Result(ResultCode.INDEX_TOUT_ERROR);

        //存入到数据库中
        ExceptionLogs logsDate = this.createExceptionLogs(e,result);
        exceptionLogsService.addExceptionLogs(logsDate);

        return result;
    }

    // redis异常
    @ExceptionHandler(PoolException.class)
    public Result PoolException(PoolException e){
        // 打印异常信息
        log.error("### redis连接异常:{} ###", e.getMessage());

        Result  result =  new Result(ResultCode.INTERFACE_INNER_INVOKE_ERROR);

        //存入到数据库中
        ExceptionLogs logsDate = this.createExceptionLogs(e,result);
        exceptionLogsService.addExceptionLogs(logsDate);

        return result;
    }

    // IO异常
    @ExceptionHandler(IOException.class)
    public Result SQLException(IOException e){
        // 打印异常信息
        log.error("### IO异常:{} ###", e.getMessage());

        Result  result =  new Result(ResultCode.INTERFACE_INNER_INVOKE_ERROR);

        //存入到数据库中
        ExceptionLogs logsDate = this.createExceptionLogs(e,result);
        exceptionLogsService.addExceptionLogs(logsDate);

        return result;
    }

    // SQLException
    @ExceptionHandler(SQLException.class)
    public Result SQLException(SQLException e){
        // 打印异常信息
        log.error("### MYSQL连接异常:{} ###", e.getMessage());

        Result  result =  new Result(ResultCode.INTERFACE_INNER_INVOKE_ERROR);

        //存入到数据库中
        ExceptionLogs logsDate = this.createExceptionLogs(e,result);
        exceptionLogsService.addExceptionLogs(logsDate);

        return result;
    }

    // SQLException
    @ExceptionHandler(SQLDataException.class)
    public Result DataTruncation(SQLDataException e){
        // 打印异常信息
        log.error("### MYSQL数据异常:{} ###", e.getMessage());

        Result  result =  new Result(ResultCode.INTERFACE_INNER_INVOKE_ERROR);

        //存入到数据库中
        ExceptionLogs logsDate = this.createExceptionLogs(e,result);
        exceptionLogsService.addExceptionLogs(logsDate);

        return result;
    }

    // SQLException
    @ExceptionHandler(ConnectException.class)
    public Result SQLException(ConnectException e){
        // 打印异常信息
        log.error("### MYSQL连接异常:{} ###", e.getMessage());

        Result  result =  new Result(ResultCode.INTERFACE_INNER_INVOKE_ERROR);

        //存入到数据库中
        ExceptionLogs logsDate = this.createExceptionLogs(e,result);
        exceptionLogsService.addExceptionLogs(logsDate);

        return result;
    }


    /**
     * 处理所有不可知的异常
     */
    @ExceptionHandler(Exception.class)
    public Result handleOtherException(Exception e){
        //打印异常堆栈信息
//        e.printStackTrace();
        // 返回结果
        Result  result =  new Result(ResultCode.SYSTEM_INNER_ERROR);
        // 错误日志
        String errorMessage = "### 不可知的异常:{"+e.getMessage()+"} ###";

        // 数据库操作异常
        if (e.getMessage().contains("MysqlDataTruncation")){
            errorMessage = "### SQL数据操作异常:{"+e.getMessage()+"} ###";
            result = new Result(ResultCode.SYSTEM_INNER_ERROR);

        }

        //获取到ServletRequestAttributes
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        // 返回结果到浏览器
        HttpUtils.write(response,result);

        // 打印异常信息
        log.error(errorMessage);

        //存入到数据库中
        ExceptionLogs logsDate = this.createExceptionLogs(e,result);
        exceptionLogsService.addExceptionLogs(logsDate);

        return result;
    }

    // 创建异常信息
    public ExceptionLogs createExceptionLogs (Exception e,Result result){
        //发生异常时用户记录用户异常信息到数据库
        StackTraceElement stackTraceElement = e.getStackTrace()[0];

        //获取到ServletRequestAttributes
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        // 访问者的IP
        String ip = request.getHeader("x-forwarded-for") != null ?request.getHeader("x-forwarded-for"):request.getRemoteAddr();

        String username = "unknown";
        // 触发用户
        if(request.getSession().getAttribute("username") != null ){
            username = request.getSession().getAttribute("username").toString();
        }

        // 错误信息
        String errorInfo = e.toString() + ",errorMassage:" + stackTraceElement + "," + "errorLine:" + stackTraceElement.getLineNumber();//异常信息
        // UUID
        String uuid= UUID.randomUUID().toString();

        // 保存到数据库
        ExceptionLogs logsDate = new ExceptionLogs();
        logsDate.setUuid(uuid);
        logsDate.setErrorCode(result.getCode());//异常信息
        logsDate.setErrorTitle(result.getMessage()); // 错误标题
        logsDate.setErrorMessage(e.getMessage()); // 状态信息
        logsDate.setErrorDetails(errorInfo); // 错误详细信息
        logsDate.setVisitIp(ip); // 访问地址
        logsDate.setVisitUser(username);// 用户登陆用户名
        logsDate.setIsRead(0); // 是否已读
        logsDate.setIpStatis(1); // 同ip统计
        logsDate.setCreateTime(new Date()); // 创建时间
        logsDate.setUpdateTime(new Date()); // 更新时间
        return logsDate;
    }
}
