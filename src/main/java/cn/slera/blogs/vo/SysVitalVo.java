package cn.slera.blogs.vo;

/**
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2020-04-30
 * @time: 19:39
 */
public class SysVitalVo {

    private Long todayVisits;

    private Long totalVisits;

    private Long todayView;

    private Long totalView;

    private Long weekLog;

    private Long totalLog;

    private Long weekComment;

    private Long totalComment;

    public Long getTodayVisits() {
        return todayVisits;
    }

    public void setTodayVisits(Long todayVisits) {
        this.todayVisits = todayVisits;
    }

    public Long getTotalVisits() {
        return totalVisits;
    }

    public void setTotalVisits(Long totalVisits) {
        this.totalVisits = totalVisits;
    }

    public Long getTodayView() {
        return todayView;
    }

    public void setTodayView(Long todayView) {
        this.todayView = todayView;
    }

    public Long getWeekLog() {
        return weekLog;
    }

    public void setWeekLog(Long weekLog) {
        this.weekLog = weekLog;
    }

    public Long getTotalLog() {
        return totalLog;
    }

    public void setTotalLog(Long totalLog) {
        this.totalLog = totalLog;
    }

    public Long getWeekComment() {
        return weekComment;
    }

    public void setWeekComment(Long weekComment) {
        this.weekComment = weekComment;
    }

    public Long getTotalComment() {
        return totalComment;
    }

    public void setTotalComment(Long totalComment) {
        this.totalComment = totalComment;
    }

    public Long getTotalView() {
        return totalView;
    }

    public void setTotalView(Long totalView) {
        this.totalView = totalView;
    }
}
