package cn.slera.blogs.vo;

/**
 *
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2020-04-30
 * @time: 12:38
 */
public class LayuiReturnVo {

    // 错误代码
    private int code;
    // 信息
    private String msg;
    // 总条数
    private int count;
    // 数据
    private Object data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
