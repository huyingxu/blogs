package cn.slera.blogs.vo;

import java.io.Serializable;

public class JsonReturnVo implements Serializable {
    // 状态
    private String state;
    // 错误代码
    private String errorCode;
    // 总数
    private Integer count;
    // 每页数据
    private Integer pageSize;
    // 结果
    private Object result;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
