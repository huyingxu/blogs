package cn.slera.blogs.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2020-05-01
 * @time: 13:28
 */
public class SysViewVo {
    private Long id;

    private String ip;

    private String createBy;

    private String ipaddr;

    private String sessionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }


    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
