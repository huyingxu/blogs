package cn.slera.blogs.vo;

/**
 *  生成验证码，并发送短信 接口
 */
public class MessageVo {

    // Session的id
    private String SessionId;
    // 类型 1 发送短信and邮件 2 仅发送短信 3. 仅发送邮箱
    private Integer type;
    // 手机号
    private String tel;
    // 邮箱账号
    private String email;
    // 标识 默认模板
    private String flag = "true";
    // 自定义签名/邮件主题
    private String sign = "验证码";
    // 邮件主题 与 sign 合并
//    private String subject;
    // 内容
    private String content;
    // 失效时间
    private Long timeout = Long.valueOf(5);
    // 用途 （默认为空, 代表验证码适用于整个系统）
    private String usedBy="";

    public String getSessionId() {
        return SessionId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Long getTimeout() {
//        if(timeout == null){ timeout = Long.valueOf(5);}
        return timeout;
    }

    public void setTimeout(Long timeout) {
        this.timeout = timeout;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUsedBy() {
        return usedBy;
    }

    public void setUsedBy(String usedBy) {
        this.usedBy = usedBy;
    }
}
