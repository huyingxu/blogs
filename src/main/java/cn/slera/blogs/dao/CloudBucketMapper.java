package cn.slera.blogs.dao;

import cn.slera.blogs.entity.CloudBucket;
import cn.slera.blogs.entity.CloudBucketExample;
import java.util.List;

public interface CloudBucketMapper {
    int insert(CloudBucket record);

    int insertSelective(CloudBucket record);

    List<CloudBucket> selectByExample(CloudBucketExample example);

    int removeBucketById(CloudBucket record);
}