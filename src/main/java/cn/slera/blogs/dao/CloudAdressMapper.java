package cn.slera.blogs.dao;

import cn.slera.blogs.entity.CloudAdress;
import cn.slera.blogs.entity.CloudAdressExample;
import java.util.List;

public interface CloudAdressMapper {
    int insert(CloudAdress record);

    int insertSelective(CloudAdress record);

    List<CloudAdress> selectByExample(CloudAdressExample example);
}