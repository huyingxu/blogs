package cn.slera.blogs.dao;

import cn.slera.blogs.dto.MessageTemplateDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

public interface CommonMapper {

    List<MessageTemplateDto> cloud_messageById(Long id);

    List<MessageTemplateDto> cloud_messageByuseBy(String useBy);

    Integer countArticle();
}
