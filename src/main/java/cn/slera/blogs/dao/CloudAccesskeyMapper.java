package cn.slera.blogs.dao;

import cn.slera.blogs.entity.CloudAccesskey;
import cn.slera.blogs.entity.CloudAccesskeyExample;
import java.util.List;

public interface CloudAccesskeyMapper {
    int insert(CloudAccesskey record);

    int insertSelective(CloudAccesskey record);

    List<CloudAccesskey> selectByExample(CloudAccesskeyExample example);
}