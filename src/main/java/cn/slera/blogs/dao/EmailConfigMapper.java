package cn.slera.blogs.dao;

import cn.slera.blogs.entity.EmailConfig;
import cn.slera.blogs.entity.EmailConfigExample;
import java.util.List;

public interface EmailConfigMapper {
    int insert(EmailConfig record);

    int insertSelective(EmailConfig record);

    List<EmailConfig> selectByExample(EmailConfigExample example);
}