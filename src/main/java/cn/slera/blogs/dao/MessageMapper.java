package cn.slera.blogs.dao;

import cn.slera.blogs.entity.Message;
import cn.slera.blogs.entity.MessageExample;
import java.util.List;

public interface MessageMapper {
    int insert(Message record);

    int insertSelective(Message record);

    List<Message> selectByExample(MessageExample example);
}