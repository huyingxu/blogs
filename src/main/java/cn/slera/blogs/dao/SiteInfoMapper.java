package cn.slera.blogs.dao;

import cn.slera.blogs.entity.SiteInfo;
import cn.slera.blogs.entity.SiteInfoExample;
import java.util.List;

public interface SiteInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SiteInfo record);

    int insertSelective(SiteInfo record);

    List<SiteInfo> selectByExample(SiteInfoExample example);

    SiteInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SiteInfo record);

    int updateByPrimaryKey(SiteInfo record);
}