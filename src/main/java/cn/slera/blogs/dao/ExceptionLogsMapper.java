package cn.slera.blogs.dao;

import cn.slera.blogs.entity.ExceptionLogs;
import cn.slera.blogs.entity.ExceptionLogsExample;
import java.util.List;

public interface ExceptionLogsMapper {
    int insert(ExceptionLogs record);

    int insertSelective(ExceptionLogs record);

    List<ExceptionLogs> selectByExample(ExceptionLogsExample example);
}