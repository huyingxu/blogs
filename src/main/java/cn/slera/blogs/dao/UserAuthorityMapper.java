package cn.slera.blogs.dao;

import cn.slera.blogs.entity.UserAuthority;
import cn.slera.blogs.entity.UserAuthorityExample;
import java.util.List;

public interface UserAuthorityMapper {
    int insert(UserAuthority record);

    int insertSelective(UserAuthority record);

    List<UserAuthority> selectByExample(UserAuthorityExample example);
}