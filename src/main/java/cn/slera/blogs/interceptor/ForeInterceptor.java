package cn.slera.blogs.interceptor;

import cn.slera.blogs.common.exception.handler.GlobalExceptionHandler;
import cn.slera.blogs.common.response.Result;
import cn.slera.blogs.common.response.ResultCode;
import cn.slera.blogs.entity.ExceptionLogs;
import cn.slera.blogs.entity.SysLog;
import cn.slera.blogs.entity.SysView;
import cn.slera.blogs.service.ExceptionLogsService;
import cn.slera.blogs.service.SysService;
import cn.slera.blogs.util.BrowserUtil;
import cn.slera.blogs.vo.SysViewVo;
import com.alibaba.fastjson.JSON;
import io.lettuce.core.RedisConnectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import cn.slera.blogs.common.exception.handler.GlobalExceptionHandler;
import org.springframework.data.redis.connection.PoolException;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import sun.net.www.http.HttpClient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * 前台拦截器
 * 说明：用于对访问数量进行统计
 *
 * @author:slera
 * @create:2019-06-20-下午 20:12
 */
public class ForeInterceptor implements HandlerInterceptor {

    @Autowired
    SysService sysService;

    @Autowired
    private ExceptionLogsService exceptionLogsService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private RestTemplate restTemplate = new RestTemplate();

    private SysLog sysLog = new SysLog();
    private SysViewVo sysView = new SysViewVo();

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    // ip查询接口
    private String dip = "http://whois.pconline.com.cn/ip.jsp?ip=";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception,PoolException,RedisConnectionException {
        // 访问者的IP
        String ip = request.getHeader("x-forwarded-for") != null ?request.getHeader("x-forwarded-for"):request.getRemoteAddr();
        // 访问地址
        String url = request.getRequestURL().toString();
        //得到用户的浏览器名
        String userbrowser = BrowserUtil.getOsAndBrowserInfo(request);
        System.out.println("日志记录url："+request.getRequestURI());
        // 给SysLog增加字段
        sysLog.setIp(StringUtils.isEmpty(ip) ? "0.0.0.0" : ip);
        sysLog.setOperateBy(StringUtils.isEmpty(userbrowser) ? "Failed to get browser name" : userbrowser);
        sysLog.setOperateUrl(StringUtils.isEmpty(url) ? "Failed to get URL" : url);
        sysLog.setCreateBy(new Date());

        // 增加访问量
        sysView.setIp(StringUtils.isEmpty(ip) ? "0.0.0.0" : ip);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        sysView.setCreateBy(df.format(new Date()));
        // 根据ip定位地址
        String ipaddr = restTemplate.getForObject(dip+ip,String.class);
        ipaddr = ipaddr.replace("\n","").replace("\r","");
        sysView.setIpaddr(ipaddr);
        sysView.setSessionId(request.getRequestedSessionId());
        // 缓存到redis
        String json = JSON.toJSONString(sysView);
        String sessionId = request.getSession().getId();
        // redis 异常直接转存mysql
        try {
            // 防止重复记录,查询sessionId是否存在redis
            String val= (String)stringRedisTemplate.opsForHash().get("platform-blogs-session",sessionId);
            // 记录存入redis
            if(val == null){
                // 临时存储数据, 计划任务执行完毕会被清除
                stringRedisTemplate.opsForHash().put("platform-blogs-session", sessionId,json);
                // 持久化数据, 计划任务执行完毕不会被清除
                stringRedisTemplate.opsForList().leftPush("platform-blogs-session-view",json);
            }
        }catch (PoolException e){// redis连接异常直接存入数据库
            StackTraceElement stackTraceElement = e.getStackTrace()[0];
            // 访问这用户名
            String username = request.getParameter("username");//用户登陆用户名
            // 错误信息
            String errorInfo = e.toString() + ",errorMassage:" + stackTraceElement + "," + "errorLine:" + stackTraceElement.getLineNumber();//异常信息
            // 标题
            String message = String.valueOf(e.getMessage());
            // UUID
            String uuid= UUID.randomUUID().toString();

            // 修改错误代码
            Result result= new Result(ResultCode.Redis_INNER_ERROR);
            // 创建异常信息
            GlobalExceptionHandler createException = new GlobalExceptionHandler();
            ExceptionLogs logsDate = createException.createExceptionLogs(e,result);
            //保存到数据库
            exceptionLogsService.addExceptionLogs(logsDate);

            // 打印异常信息
            log.error("### "+ResultCode.Redis_INNER_ERROR.message()+" ###");

            SysView sv = new SysView();
            sv.setIpaddr(sysView.getIpaddr());
            sv.setIp(sysView.getIp());
            sv.setCreateBy(new Date());
            sysService.addView(sv);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            // 保存日志信息
            sysLog.setRemark(method.getName());
            if(! sysLog.getOperateUrl().contains("error")){
                sysService.addLog(sysLog);
            }

        } else {
            String uri = request.getRequestURI();
            sysLog.setRemark(uri);
            sysService.addLog(sysLog);
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
