package cn.slera.blogs.dto;

import cn.slera.blogs.entity.CloudAccesskey;

public class MessageTemplateDto {

    private Integer id;

    private String templateId;

    private String sign;

    private Integer keyId;

    private String appId;

    private String region;

    private String status;

    private String template_content;

    private CloudAccesskey cloudAccesskey;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public Integer getKeyId() {
        return keyId;
    }

    public void setKeyId(Integer keyId) {
        this.keyId = keyId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTemplate_content() {
        return template_content;
    }

    public void setTemplate_content(String template_content) {
        this.template_content = template_content;
    }

    public CloudAccesskey getCloudAccesskey() {
        return cloudAccesskey;
    }

    public void setCloudAccesskey(CloudAccesskey cloudAccesskey) {
        this.cloudAccesskey = cloudAccesskey;
    }
}
