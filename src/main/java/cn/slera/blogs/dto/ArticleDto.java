package cn.slera.blogs.dto;

import cn.slera.blogs.entity.ArticleCategory;
import cn.slera.blogs.entity.CategoryInfo;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

/**
 * 文章信息类
 * 说明：关联了t_article_info/t_article_content/t_article_category/t_article_picture/
 * t_article_picture五张表的基础字段
 *
 * @author:slera
 * @create:2019-06-19-下午 14:13
 */
public class ArticleDto {

    // t_article_info基础字段
    private Long id;                // 主键
    private String title;           // 文章标题
    private String summary;         // 文章简介
    private Boolean isTop;          // 文章是否置顶
    private Integer traffic;        // 文章浏览量
    private Date createBy;          // 文章创建时间
    private Integer isUsed;         // 文章状态  0 待发布 1 发布  2 删除

    // t_article_content基础字段
    private Long articleContentId;  // ArticleContent表主键
    private String content;         // 文章内容

    // t_article_picture基础字段
    private Long categoryId;        // 分类ID
    private String categoryName;    // 分类名称
    private Byte categoryNumber;    // 分类对应的数量

    // t_article_category基础字段
    private Long articleCategoryId; // ArticleCategory表主键

    // t_article_picture基础字段
    private Long articlePictureId;  // ArticlePicture表主键
    private String pictureUrl;      // 文章题图url

    private List<CategoryInfo> categoryInfo; // 分类信息
    private List<ArticleCategory> articleCategory; // 主键

    private String categoryNames; //所有分类，以,拼接

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Date createBy) {
        this.createBy = createBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Boolean getTop() {
        return isTop;
    }

    public void setTop(Boolean top) {
        isTop = top;
    }

    public Integer getTraffic() {
        return traffic;
    }

    public void setTraffic(Integer traffic) {
        this.traffic = traffic;
    }

    public Long getArticleContentId() {
        return articleContentId;
    }

    public void setArticleContentId(Long articleContentId) {
        this.articleContentId = articleContentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Byte getCategoryNumber() {
        return categoryNumber;
    }

    public void setCategoryNumber(Byte categoryNumber) {
        this.categoryNumber = categoryNumber;
    }

    public Long getArticlePictureId() {
        return articlePictureId;
    }

    public void setArticlePictureId(Long articlePictureId) {
        this.articlePictureId = articlePictureId;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Long getArticleCategoryId() {
        return articleCategoryId;
    }

    public void setArticleCategoryId(Long articleCategoryId) {
        this.articleCategoryId = articleCategoryId;
    }

    public List<CategoryInfo> getCategoryInfo() {
        return categoryInfo;
    }

    public void setCategoryInfo(List<CategoryInfo> categoryInfo) {
        this.categoryInfo = categoryInfo;
    }

    public List<cn.slera.blogs.entity.ArticleCategory> getArticleCategory() {
        return articleCategory;
    }

    public void setArticleCategory(List<ArticleCategory> articleCategory) {
        articleCategory = articleCategory;
    }

    public String getCategoryNames() { return categoryNames; }

    public void setCategoryNames(String categoryNames) { this.categoryNames = categoryNames; }

    public Integer getIsUsed() { return isUsed; }

    public void setIsUsed(Integer isUsed) { this.isUsed = isUsed; }
}
