package cn.slera.blogs.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 带题图信息的文章基础信息分装类
 *
 * @author:小胡子呀
 * @create:2019-06-19-下午 14:53
 */
public class ArticleWithPictureDto {
    // t_article_info基础字段
    private Long id;                    // ArticleInfo表主键
    private String title;               // 文章标题
    private String summary;             // 文章简介
    private Boolean isTop;              // 文章是否置顶
    private Integer traffic;            // 文章阅读量
    private Date create_by;           // 文章创建时间
    private Integer isUsed;           // 文章状态  0 待发布 1 发布  2 删除

    // t_article_picture基础字段
    private Long articlePictureId;      // ArticlePicture主键
    private String pictureUrl;          // 文章题图URL

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Boolean getTop() {
        return isTop;
    }

    public void setTop(Boolean top) {
        isTop = top;
    }

    public Integer getTraffic() {
        return traffic;
    }

    public void setTraffic(Integer traffic) {
        this.traffic = traffic;
    }

    public Long getArticlePictureId() {
        return articlePictureId;
    }

    public void setArticlePictureId(Long articlePictureId) {
        this.articlePictureId = articlePictureId;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getCreate_by() {
        return create_by;
    }

    public void setCreate_by(Date create_by) {
        this.create_by = create_by;
    }

    public Integer getIsUsed() { return isUsed; }

    public void setIsUsed(Integer isUsed) { this.isUsed = isUsed; }
}
