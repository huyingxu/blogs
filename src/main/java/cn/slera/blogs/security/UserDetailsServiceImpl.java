package cn.slera.blogs.security;


import cn.slera.blogs.common.response.Result;
import cn.slera.blogs.common.response.ResultCode;
import cn.slera.blogs.entity.view.ViewUserAuthority;
import cn.slera.blogs.service.ShareService;
import cn.slera.blogs.util.HttpUtils;
import cn.slera.blogs.vo.JsonReturnVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import cn.slera.blogs.entity.User;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * 用户登录认证信息查询
 * @author Louis
 * @date Jun 29, 2019
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private ShareService shareService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = shareService.findByUsername(username);
        //获取到ServletRequestAttributes 中的session
        ServletRequestAttributes att = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
        HttpServletResponse response = att.getResponse();
        HttpServletRequest request =  att.getRequest();
        if (user == null) {
            // 认证失败后的一些处理。
            try {
                HttpUtils.write(response,401,new Result(ResultCode.USER_NOT_EXIST));
                return null;
            } catch (IOException e) {
                // 人为异常，不抛出
//                e.printStackTrace();
            }
        }
        request.getSession().setAttribute("username",user.getUsername());
        request.getSession().setAttribute("cname",user.getCname());
        // 用户权限列表，根据用户拥有的权限标识与如 @PreAuthorize("hasAuthority('sys:menu:view')") 标注的接口对比，决定是否可以调用接口
        List<ViewUserAuthority> permissions = shareService.findAuthorityByUsername(username);
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        if (permissions.size() != 0){
            authorities.add(new SimpleGrantedAuthority("ROLE_" + permissions.get(0).getAuthorityName()));
        }else{
            //用户权限为空，设为普通用户
            authorities.add(new SimpleGrantedAuthority("ROLE_COMMON"));
        }
        return new JwtUserDetails(username, user.getPassword(), authorities);
    }
}
