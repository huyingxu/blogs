package cn.slera.blogs.filter;

import cn.slera.blogs.entity.User;
import cn.slera.blogs.security.JwtAuthenticatioToken;
import cn.slera.blogs.service.ShareService;
import cn.slera.blogs.util.BodyUtil;
import cn.slera.blogs.util.HttpUtils;
import cn.slera.blogs.util.JwtTokenUtils;
import cn.slera.blogs.util.RedisUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * 启动登录认证流程过滤器
 * @author Louis
 * @date Jun 29, 2019
 */
@Service
public class JwtLoginFilter extends UsernamePasswordAuthenticationFilter {

	public JwtLoginFilter(AuthenticationManager authManager) {
        setAuthenticationManager(authManager);
    }

	@Autowired
	RedisUtil redisUtil;

	@Autowired
	private ShareService shareService;

//	@Override
//	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
//			throws IOException, ServletException {
//		// POST 请求 /login 登录时拦截， 由此方法触发执行登录认证流程，可以在此覆写整个登录认证逻辑
//		super.doFilter(req, res, chain);
//	}
private static JwtLoginFilter atoboPipeline;

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
		// 可以在此覆写尝试进行登录认证的逻辑，登录成功之后等操作不再此方法内
		// 如果使用此过滤器来触发登录认证流程，注意登录请求数据格式的问题
		// 此过滤器的用户名密码默认从request.getParameter()获取，但是这种
		// 读取方式不能读取到如 application/json 等 post 请求数据，需要把
		// 用户名密码的读取逻辑修改为到流中读取request.getInputStream()
		BodyUtil requestBody= new BodyUtil();
		String body = requestBody.getBody(request);
		if(body.equals("")){
			return null;
		}
		JSONObject jsonObject = JSON.parseObject(body);
		// 获取登录类型
		String login_type = jsonObject.getString("login_type");
		Authentication a = null;
		if("1".equals(login_type)){
			String username = jsonObject.getString("username");
			String password = jsonObject.getString("password");
			JwtAuthenticatioToken authRequest = new JwtAuthenticatioToken(username, password);

			// Allow subclasses to set the "details" property
			setDetails(request, authRequest);
			try {
				a= this.getAuthenticationManager().authenticate(authRequest);
			}catch (AuthenticationException e){

			}
		}
		else if("2".equals(login_type)){
			String phone = jsonObject.getString("phone");
			String code = jsonObject.getString("code");
//			JwtAuthenticatioToken authRequest = new JwtAuthenticatioToken(phone, code,login_type);
//
//			// Allow subclasses to set the "details" property
//			setDetails(request, authRequest);
//			try {
//				a= this.getAuthenticationManager().authenticate(authRequest);
//			}catch (AuthenticationException e){
//
//			}
			String login_code= (String)request.getSession().getAttribute("login_code");
//			String data= stringRedisTemplate.opsForValue().get("loginCode_"+request.getRequestedSessionId());
			if(login_code.equals(code)){
				// 从user表中取出
				User user = shareService.findByPhone(phone);
			UsernamePasswordAuthenticationToken authRequest0 = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

			}else{
				System.out.println("认证shibai");
			}
		}
		return a;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
		// 存储登录认证信息到上下文
		SecurityContextHolder.getContext().setAuthentication(authResult);
		// 记住我服务
		getRememberMeServices().loginSuccess(request, response, authResult);
		// 触发事件监听器
		if (this.eventPublisher != null) {
			eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(authResult, this.getClass()));
		}
		// 生成并返回token给客户端，后续访问携带此token
		JwtAuthenticatioToken token = new JwtAuthenticatioToken(null, null, JwtTokenUtils.generateToken(authResult));

		response.setHeader("Authorization","Bearer "+token.getToken());
//		redisUtils.set(token_format,userInfo,globalConfig.getTokenExpires());
		request.getSession().setAttribute("Authorization",token.getToken());
//		request.getRequestDispatcher("/saveToken").forward(request,response);
		HttpUtils.write(response,200,token);
	}
}
