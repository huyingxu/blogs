package cn.slera.blogs.entity;

public class CloudBucket {
    private Integer id;

    private String bucketName;

    private Integer addrId;

    private Integer keyId;

    private String status;

    // 一对一 阿里云地址
    private CloudAdress cloudAdress;
    // 一对一 key值
    private  CloudAccesskey cloudAccesskey;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName == null ? null : bucketName.trim();
    }

    public Integer getAddrId() {
        return addrId;
    }

    public void setAddrId(Integer addrId) {
        this.addrId = addrId;
    }

    public Integer getKeyId() {
        return keyId;
    }

    public void setKeyId(Integer keyId) {
        this.keyId = keyId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public CloudAdress getCloudAdress() {
        return cloudAdress;
    }

    public void setCloudAdress(CloudAdress cloudAdress) {
        this.cloudAdress = cloudAdress;
    }

    public CloudAccesskey getCloudAccesskey() { return cloudAccesskey; }

    public void setCloudAccesskey(CloudAccesskey cloudAccesskey) {
        this.cloudAccesskey = cloudAccesskey;
    }
}
