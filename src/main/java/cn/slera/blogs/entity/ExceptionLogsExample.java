package cn.slera.blogs.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExceptionLogsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ExceptionLogsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andErrorCodeIsNull() {
            addCriterion("error_code is null");
            return (Criteria) this;
        }

        public Criteria andErrorCodeIsNotNull() {
            addCriterion("error_code is not null");
            return (Criteria) this;
        }

        public Criteria andErrorCodeEqualTo(Integer value) {
            addCriterion("error_code =", value, "errorCode");
            return (Criteria) this;
        }

        public Criteria andErrorCodeNotEqualTo(Integer value) {
            addCriterion("error_code <>", value, "errorCode");
            return (Criteria) this;
        }

        public Criteria andErrorCodeGreaterThan(Integer value) {
            addCriterion("error_code >", value, "errorCode");
            return (Criteria) this;
        }

        public Criteria andErrorCodeGreaterThanOrEqualTo(Integer value) {
            addCriterion("error_code >=", value, "errorCode");
            return (Criteria) this;
        }

        public Criteria andErrorCodeLessThan(Integer value) {
            addCriterion("error_code <", value, "errorCode");
            return (Criteria) this;
        }

        public Criteria andErrorCodeLessThanOrEqualTo(Integer value) {
            addCriterion("error_code <=", value, "errorCode");
            return (Criteria) this;
        }

        public Criteria andErrorCodeIn(List<Integer> values) {
            addCriterion("error_code in", values, "errorCode");
            return (Criteria) this;
        }

        public Criteria andErrorCodeNotIn(List<Integer> values) {
            addCriterion("error_code not in", values, "errorCode");
            return (Criteria) this;
        }

        public Criteria andErrorCodeBetween(Integer value1, Integer value2) {
            addCriterion("error_code between", value1, value2, "errorCode");
            return (Criteria) this;
        }

        public Criteria andErrorCodeNotBetween(Integer value1, Integer value2) {
            addCriterion("error_code not between", value1, value2, "errorCode");
            return (Criteria) this;
        }

        public Criteria andErrorTitleIsNull() {
            addCriterion("error_title is null");
            return (Criteria) this;
        }

        public Criteria andErrorTitleIsNotNull() {
            addCriterion("error_title is not null");
            return (Criteria) this;
        }

        public Criteria andErrorTitleEqualTo(String value) {
            addCriterion("error_title =", value, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleNotEqualTo(String value) {
            addCriterion("error_title <>", value, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleGreaterThan(String value) {
            addCriterion("error_title >", value, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleGreaterThanOrEqualTo(String value) {
            addCriterion("error_title >=", value, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleLessThan(String value) {
            addCriterion("error_title <", value, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleLessThanOrEqualTo(String value) {
            addCriterion("error_title <=", value, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleLike(String value) {
            addCriterion("error_title like", value, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleNotLike(String value) {
            addCriterion("error_title not like", value, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleIn(List<String> values) {
            addCriterion("error_title in", values, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleNotIn(List<String> values) {
            addCriterion("error_title not in", values, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleBetween(String value1, String value2) {
            addCriterion("error_title between", value1, value2, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorTitleNotBetween(String value1, String value2) {
            addCriterion("error_title not between", value1, value2, "errorTitle");
            return (Criteria) this;
        }

        public Criteria andErrorMessageIsNull() {
            addCriterion("error_message is null");
            return (Criteria) this;
        }

        public Criteria andErrorMessageIsNotNull() {
            addCriterion("error_message is not null");
            return (Criteria) this;
        }

        public Criteria andErrorMessageEqualTo(String value) {
            addCriterion("error_message =", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageNotEqualTo(String value) {
            addCriterion("error_message <>", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageGreaterThan(String value) {
            addCriterion("error_message >", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageGreaterThanOrEqualTo(String value) {
            addCriterion("error_message >=", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageLessThan(String value) {
            addCriterion("error_message <", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageLessThanOrEqualTo(String value) {
            addCriterion("error_message <=", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageLike(String value) {
            addCriterion("error_message like", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageNotLike(String value) {
            addCriterion("error_message not like", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageIn(List<String> values) {
            addCriterion("error_message in", values, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageNotIn(List<String> values) {
            addCriterion("error_message not in", values, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageBetween(String value1, String value2) {
            addCriterion("error_message between", value1, value2, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageNotBetween(String value1, String value2) {
            addCriterion("error_message not between", value1, value2, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsIsNull() {
            addCriterion("error_details is null");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsIsNotNull() {
            addCriterion("error_details is not null");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsEqualTo(String value) {
            addCriterion("error_details =", value, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsNotEqualTo(String value) {
            addCriterion("error_details <>", value, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsGreaterThan(String value) {
            addCriterion("error_details >", value, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsGreaterThanOrEqualTo(String value) {
            addCriterion("error_details >=", value, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsLessThan(String value) {
            addCriterion("error_details <", value, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsLessThanOrEqualTo(String value) {
            addCriterion("error_details <=", value, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsLike(String value) {
            addCriterion("error_details like", value, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsNotLike(String value) {
            addCriterion("error_details not like", value, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsIn(List<String> values) {
            addCriterion("error_details in", values, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsNotIn(List<String> values) {
            addCriterion("error_details not in", values, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsBetween(String value1, String value2) {
            addCriterion("error_details between", value1, value2, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andErrorDetailsNotBetween(String value1, String value2) {
            addCriterion("error_details not between", value1, value2, "errorDetails");
            return (Criteria) this;
        }

        public Criteria andIsReadIsNull() {
            addCriterion("is_read is null");
            return (Criteria) this;
        }

        public Criteria andIsReadIsNotNull() {
            addCriterion("is_read is not null");
            return (Criteria) this;
        }

        public Criteria andIsReadEqualTo(Integer value) {
            addCriterion("is_read =", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadNotEqualTo(Integer value) {
            addCriterion("is_read <>", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadGreaterThan(Integer value) {
            addCriterion("is_read >", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_read >=", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadLessThan(Integer value) {
            addCriterion("is_read <", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadLessThanOrEqualTo(Integer value) {
            addCriterion("is_read <=", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadIn(List<Integer> values) {
            addCriterion("is_read in", values, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadNotIn(List<Integer> values) {
            addCriterion("is_read not in", values, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadBetween(Integer value1, Integer value2) {
            addCriterion("is_read between", value1, value2, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadNotBetween(Integer value1, Integer value2) {
            addCriterion("is_read not between", value1, value2, "isRead");
            return (Criteria) this;
        }

        public Criteria andVisitIpIsNull() {
            addCriterion("visit_ip is null");
            return (Criteria) this;
        }

        public Criteria andVisitIpIsNotNull() {
            addCriterion("visit_ip is not null");
            return (Criteria) this;
        }

        public Criteria andVisitIpEqualTo(String value) {
            addCriterion("visit_ip =", value, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpNotEqualTo(String value) {
            addCriterion("visit_ip <>", value, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpGreaterThan(String value) {
            addCriterion("visit_ip >", value, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpGreaterThanOrEqualTo(String value) {
            addCriterion("visit_ip >=", value, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpLessThan(String value) {
            addCriterion("visit_ip <", value, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpLessThanOrEqualTo(String value) {
            addCriterion("visit_ip <=", value, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpLike(String value) {
            addCriterion("visit_ip like", value, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpNotLike(String value) {
            addCriterion("visit_ip not like", value, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpIn(List<String> values) {
            addCriterion("visit_ip in", values, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpNotIn(List<String> values) {
            addCriterion("visit_ip not in", values, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpBetween(String value1, String value2) {
            addCriterion("visit_ip between", value1, value2, "visitIp");
            return (Criteria) this;
        }

        public Criteria andVisitIpNotBetween(String value1, String value2) {
            addCriterion("visit_ip not between", value1, value2, "visitIp");
            return (Criteria) this;
        }

        public Criteria andIpStatisIsNull() {
            addCriterion("ip_statis is null");
            return (Criteria) this;
        }

        public Criteria andIpStatisIsNotNull() {
            addCriterion("ip_statis is not null");
            return (Criteria) this;
        }

        public Criteria andIpStatisEqualTo(Integer value) {
            addCriterion("ip_statis =", value, "ipStatis");
            return (Criteria) this;
        }

        public Criteria andIpStatisNotEqualTo(Integer value) {
            addCriterion("ip_statis <>", value, "ipStatis");
            return (Criteria) this;
        }

        public Criteria andIpStatisGreaterThan(Integer value) {
            addCriterion("ip_statis >", value, "ipStatis");
            return (Criteria) this;
        }

        public Criteria andIpStatisGreaterThanOrEqualTo(Integer value) {
            addCriterion("ip_statis >=", value, "ipStatis");
            return (Criteria) this;
        }

        public Criteria andIpStatisLessThan(Integer value) {
            addCriterion("ip_statis <", value, "ipStatis");
            return (Criteria) this;
        }

        public Criteria andIpStatisLessThanOrEqualTo(Integer value) {
            addCriterion("ip_statis <=", value, "ipStatis");
            return (Criteria) this;
        }

        public Criteria andIpStatisIn(List<Integer> values) {
            addCriterion("ip_statis in", values, "ipStatis");
            return (Criteria) this;
        }

        public Criteria andIpStatisNotIn(List<Integer> values) {
            addCriterion("ip_statis not in", values, "ipStatis");
            return (Criteria) this;
        }

        public Criteria andIpStatisBetween(Integer value1, Integer value2) {
            addCriterion("ip_statis between", value1, value2, "ipStatis");
            return (Criteria) this;
        }

        public Criteria andIpStatisNotBetween(Integer value1, Integer value2) {
            addCriterion("ip_statis not between", value1, value2, "ipStatis");
            return (Criteria) this;
        }

        public Criteria andVisitUserIsNull() {
            addCriterion("visit_user is null");
            return (Criteria) this;
        }

        public Criteria andVisitUserIsNotNull() {
            addCriterion("visit_user is not null");
            return (Criteria) this;
        }

        public Criteria andVisitUserEqualTo(String value) {
            addCriterion("visit_user =", value, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserNotEqualTo(String value) {
            addCriterion("visit_user <>", value, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserGreaterThan(String value) {
            addCriterion("visit_user >", value, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserGreaterThanOrEqualTo(String value) {
            addCriterion("visit_user >=", value, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserLessThan(String value) {
            addCriterion("visit_user <", value, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserLessThanOrEqualTo(String value) {
            addCriterion("visit_user <=", value, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserLike(String value) {
            addCriterion("visit_user like", value, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserNotLike(String value) {
            addCriterion("visit_user not like", value, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserIn(List<String> values) {
            addCriterion("visit_user in", values, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserNotIn(List<String> values) {
            addCriterion("visit_user not in", values, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserBetween(String value1, String value2) {
            addCriterion("visit_user between", value1, value2, "visitUser");
            return (Criteria) this;
        }

        public Criteria andVisitUserNotBetween(String value1, String value2) {
            addCriterion("visit_user not between", value1, value2, "visitUser");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}