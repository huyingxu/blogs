package cn.slera.blogs.entity;

public class CloudAdress {
    private Integer id;

    private String regionlocation;

    private String endpoint;

    private String regionnames;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegionlocation() {
        return regionlocation;
    }

    public void setRegionlocation(String regionlocation) {
        this.regionlocation = regionlocation == null ? null : regionlocation.trim();
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint == null ? null : endpoint.trim();
    }

    public String getRegionnames() {
        return regionnames;
    }

    public void setRegionnames(String regionnames) {
        this.regionnames = regionnames == null ? null : regionnames.trim();
    }
}