package cn.slera.blogs.entity;

public class CloudAccesskey {
    private Integer id;

    private String accesskeyId;

    private String accesskeySecret;

    private String status;

    private String useby;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccesskeyId() {
        return accesskeyId;
    }

    public void setAccesskeyId(String accesskeyId) {
        this.accesskeyId = accesskeyId == null ? null : accesskeyId.trim();
    }

    public String getAccesskeySecret() {
        return accesskeySecret;
    }

    public void setAccesskeySecret(String accesskeySecret) {
        this.accesskeySecret = accesskeySecret == null ? null : accesskeySecret.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getUseby() {
        return useby;
    }

    public void setUseby(String useby) {
        this.useby = useby == null ? null : useby.trim();
    }
}