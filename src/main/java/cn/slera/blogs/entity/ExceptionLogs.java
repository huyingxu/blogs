package cn.slera.blogs.entity;

import java.util.Date;

public class ExceptionLogs {
    private String uuid;

    private Integer errorCode;

    private String errorTitle;

    private String errorMessage;

    private String errorDetails;

    private Integer isRead;

    private String visitIp;

    private Integer ipStatis;

    private String visitUser;

    private Date createTime;

    private Date updateTime;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorTitle() {
        return errorTitle;
    }

    public void setErrorTitle(String errorTitle) {
        this.errorTitle = errorTitle == null ? null : errorTitle.trim();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage == null ? null : errorMessage.trim();
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails == null ? null : errorDetails.trim();
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public String getVisitIp() {
        return visitIp;
    }

    public void setVisitIp(String visitIp) {
        this.visitIp = visitIp == null ? null : visitIp.trim();
    }

    public Integer getIpStatis() {
        return ipStatis;
    }

    public void setIpStatis(Integer ipStatis) {
        this.ipStatis = ipStatis;
    }

    public String getVisitUser() {
        return visitUser;
    }

    public void setVisitUser(String visitUser) {
        this.visitUser = visitUser == null ? null : visitUser.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}