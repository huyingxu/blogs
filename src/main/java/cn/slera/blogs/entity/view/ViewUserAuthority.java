package cn.slera.blogs.entity.view;

/**
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2020-03-21
 * @time: 19:43
 */
public class ViewUserAuthority {

    // 用户名
    private String username;
    // 权限名
    private String cname;
    // 权限英文名
    private String authorityName;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }
}
