package cn.slera.blogs.config.task;

import cn.slera.blogs.common.exception.handler.GlobalExceptionHandler;
import cn.slera.blogs.controller.LoginController;
import cn.slera.blogs.service.SysService;
import org.slf4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 计划任务
 *
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2020-04-12
 * @time: 13:18
 */
public class SysTack extends QuartzJobBean {

    @Autowired
    SysService sysService;

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("SysViewTask--------开始时间:{}", sdf.format(new Date()));

        //将 Redis 里的记录的地址信息同步到数据库里
        sysService.transSysViewFromRedisToMysql();
    }
}
