package cn.slera.blogs.config.task;

import cn.slera.blogs.service.MessageService;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WeatherTack extends QuartzJobBean {

    @Autowired
    MessageService messageService;

    private static final Logger log = Logger.getLogger(SysTack.class);

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        log.info("WeatherTack--------开始时间:{}"+sdf.format(new Date()));
        // 向用户发送问候语
        log.info(messageService.sendGreetings());
    }
}
