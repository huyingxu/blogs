package cn.slera.blogs.config;

import cn.slera.blogs.filter.JwtAuthenticationFilter;
import cn.slera.blogs.provider.JwtAuthenticationProvider;
import cn.slera.blogs.filter.JwtLoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

/**
 * @description: 定制请求授权规则
 *
 * @return: 
 * @author: Mr.hu
 * @create: 2020-03-22 20:44
 */ 

@Configuration
@EnableWebSecurity //开启webSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationFailureHandler ctwAuthenticationFailureHandler;
    
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 使用自定义登录身份认证组件
        auth.authenticationProvider(new JwtAuthenticationProvider(userDetailsService));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 允许同源的iframe页面嵌套
        http.headers().frameOptions().sameOrigin();
        // 禁用 csrf, 由于使用的是JWT，我们这里不需要csrf
        http.cors().and().csrf().disable()
            .authorizeRequests()
            // 跨域预检请求
            .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
            // 登录URL
            // 方案一 ：配置不拦截请求，其他所有请求需要身份认证
            .antMatchers("/js/**","/js/**/**","/css/**","/img/**","/images/**","/webjars/**","/fonts/**","/layer/**","/layuiadmin/**").permitAll() // 开放静态资源
            .antMatchers("/login","/test/**","/*","/swagger**/**","/v2/**","/api/**/**","/logout").permitAll()
            .anyRequest().authenticated();
            // 方案二 ： 配置需要认证的请求，其他请求不需要身份认证 (因为这里使用了springboot + thymeleaf，方案一比较复杂)
//            .antMatchers("/api/**/**","/api/**","/oss/**").authenticated()
//            .anyRequest().permitAll();
        //访问被拒绝自动跳转到登录页面
        http.formLogin().loginPage("/login").failureHandler(ctwAuthenticationFailureHandler);

        // 退出登录处理器
        http.logout().logoutSuccessUrl("/login");
        // 开启登录认证流程过滤器
        http.addFilterBefore(new JwtLoginFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class);
        // 访问控制时登录状态检查过滤器
        http.addFilterBefore(new JwtAuthenticationFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }
    
}
