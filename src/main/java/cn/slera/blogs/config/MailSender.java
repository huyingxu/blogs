package cn.slera.blogs.config;

import cn.slera.blogs.dao.EmailConfigMapper;
import cn.slera.blogs.entity.EmailConfig;
import cn.slera.blogs.entity.EmailConfigExample;
import cn.slera.blogs.vo.JsonReturnVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.mail.internet.MimeMessage;

@Component
public class MailSender extends JavaMailSenderImpl {

    @Autowired
    private EmailConfigMapper emailConfigMapper;
    @Autowired
    private JavaMailSender mailSender;

    private static MailSender emailDataSource = null;

    private MailSender(){};

    private MailSender getInstance(){
        if (emailDataSource==null) {
            synchronized (MailSender.class){
                if (emailDataSource==null){
                    emailDataSource = new MailSender();
                }
            }
        }
        return emailDataSource;
    }

    /**
     * 首次加载初始化emailDataSource对象
     */
    @PostConstruct
    public void init() {
        emailDataSource = this;
        emailDataSource.mailSender = this.mailSender;
        emailDataSource.emailConfigMapper = this.emailConfigMapper;
    }

    /**
     * @Description: 修改邮件端配置项
     */
    public JsonReturnVo changeDataSource(EmailConfig emailConfig,JsonReturnVo result){
        try {
            //使用默认配置
            if (emailConfig ==null && emailConfig.getUsername() == null ){
                MimeMessage message = mailSender.createMimeMessage();
                EmailConfigExample example = new EmailConfigExample();
                example.or().andStatusEqualTo("2");
                emailConfig= emailConfigMapper.selectByExample(example).get(0);
            }
            EmailConfig email = emailConfig;
            emailDataSource.setHost(email.getHost());
            emailDataSource.setPassword(email.getPassword());
            emailDataSource.setUsername(email.getUsername());
            emailDataSource.setPort(email.getPort());
            emailDataSource.setDefaultEncoding("utf8");
            this.mailSender = emailDataSource;
        }catch (NullPointerException e){
            result.setState("error");
            result.setResult( "Incorrect mail configuration parameters!");
        }catch (Exception e){
            result.setState("error");
            result.setResult( e);
            e.printStackTrace();
        }
        return result;

    }


}

