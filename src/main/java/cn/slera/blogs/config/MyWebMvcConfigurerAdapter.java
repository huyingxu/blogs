package cn.slera.blogs.config;

import cn.slera.blogs.interceptor.ForeInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.*;

import java.nio.charset.Charset;
import java.util.List;

/**
 * 说明：更改编码为UTF-8
 *
 * @author:小胡子呀
 * @create:20120-01-02-上午 10:49
 */
@Configuration
public class MyWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {

    /**
     *
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/up").setViewName("/oss/upload");
        registry.addViewController("/admin/home").setViewName("/admin/home");
        super.addViewControllers(registry);
    }

    @Bean
    ForeInterceptor foreInterceptor() {
        return new ForeInterceptor();
    }

    /**
     * 拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 静态资源不记录到数据库
        registry.addInterceptor(foreInterceptor()).addPathPatterns("/**").excludePathPatterns("/js/**", "/css/**","/img/**","/images/**","/webjars/**","/layuiadmin/**","/fonts/**","/layer/**","/layui/**","/admin/assets/**","/error");// 静态资源不统计访问记录
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
//        registry.addInterceptor(getBackInterceptor()).addPathPatterns("/admin/**","/cms/**").excludePathPatterns("/login", "/admin/login");
//        registry.addInterceptor(getForeInterceptor()).addPathPatterns("/**").excludePathPatterns("/login", "/admin/**", "/js/**", "/css/**", "/img/**","/bootstrap/**","/layer/**");
        super.addInterceptors(registry);
    }
    @Bean
    public HttpMessageConverter<String> responseBodyConverter() {
        StringHttpMessageConverter converter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
        return converter;
    }

    @Bean
    public HandlerInterceptor getForeInterceptor() {
        return new ForeInterceptor();
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        super.configureMessageConverters(converters);
        converters.add(responseBodyConverter());
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false);
    }

}

