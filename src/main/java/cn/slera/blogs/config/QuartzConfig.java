package cn.slera.blogs.config;

import cn.slera.blogs.config.task.SysTack;
import cn.slera.blogs.config.task.WeatherTack;
import org.quartz.*;
import static org.quartz.CronScheduleBuilder.*;
import static org.quartz.TriggerBuilder.*;
import static org.quartz.CronScheduleBuilder.*;
import static org.quartz.DateBuilder.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2020-04-12
 * @time: 12:58
 */
@Configuration
public class QuartzConfig {

    @Value("${quartz.job-SysView.idenitty}")
    private String LIKE_TASK_IDENTITY;

    @Value("${quartz.job-SysView.enable}")
    private Boolean enable;

    @Value("${quartz.job-SysView.condition}")
    private String condition;

    @Value("${quartz.job-SysView.condition-daily}")
    private String conditionDaily;

    @Bean
    public JobDetail quartzDetail(){
        return JobBuilder.newJob(SysTack.class).withIdentity(LIKE_TASK_IDENTITY).storeDurably().build();
    }

    @Bean
    public JobDetail weatherTackDetail(){
        return JobBuilder.newJob(WeatherTack.class).withIdentity("WeatherTack").storeDurably().build();
    }

    @Bean
    public Trigger quartzTrigger(){
        if(enable){
            // 简单计划生成器
//        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
//                .withIntervalInSeconds(10)  //设置时间周期单位秒
//                .withIntervalInHours(2)  //两个小时执行一次
//                .repeatForever().withMisfireHandlingInstructionNextWithExistingCount();//在暂停此任务操作一段时间后,在恢复任务,则会将暂停期间未执行的任务全部补充上,目的是暂停期间的任务忽略掉,不要补充执行.
            CronScheduleBuilder term;
            // 周期计划
            if(conditionDaily.contains(":")){// 每天固定时间执行
                String[] strArr = conditionDaily.split(":");
                term = dailyAtHourAndMinute(Integer.parseInt(strArr[0]),Integer.parseInt(strArr[1]));
            }else{//  条件  分 时 日 月 周
                term = cronSchedule(condition);
            }
            // 定时任务1： 同步访问日志
            TriggerBuilder.newTrigger().forJob(quartzDetail())
                    .withIdentity(LIKE_TASK_IDENTITY)
//                .withSchedule(dailyAtHourAndMinute(4,30))// 每天固定时间执行
//                .withSchedule(scheduleBuilder) // 使用简单计划生成器
                    .withSchedule(term)
                    .build();

            // 定时任务2： 天气问候语
            return TriggerBuilder.newTrigger().forJob(weatherTackDetail())
                    .withIdentity("WeatherTack")
                    .withSchedule(dailyAtHourAndMinute(8,00))// 每天固定时间执行
                    .build();
        }
        return null;
    }
}
