package cn.slera.blogs.controller;

import cn.slera.blogs.entity.SiteInfo;
import cn.slera.blogs.entity.User;
import cn.slera.blogs.service.UserService;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/cms")
public class UserController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserService userService;

    /**
     * 后台首页
     *
     * @param
     * @return
     */
    @GetMapping("/index")
    public String home() {
        return "/admin/index";
    }

    /**
     * 获取访问量
     *
     * @param
     * @return
     */
    @GetMapping("/getCount")
    @ResponseBody
    public String count(){
        long count = 1;
        if(stringRedisTemplate.hasKey("platform-blogs-count")){
            count = Long.valueOf(stringRedisTemplate.opsForValue().get("platform-blogs-count")) + 1;
        }
        stringRedisTemplate.opsForValue().set("blogs_count", String.valueOf(count));
        return String.valueOf(count);
    }

    /**
     *  获取网站基础信息
     *
     * @param
     * @return
     */
    @GetMapping("/siteInfo")
    public ModelAndView siteInfo(){
        ModelAndView mv = new ModelAndView();
//        if(stringRedisTemplate.hasKey("basic_info")){
            String info = stringRedisTemplate.opsForValue().get("platform-blogs-info");
//        }
        SiteInfo siteInfo = JSON.parseObject(info, SiteInfo.class);
        mv.addObject("siteInfo",siteInfo);
        mv.setViewName("/admin/setup/info");
        return mv;
    }

    /**
     *  修改站点信息
     *
     * @param
     * @return
     */
    @PostMapping("/addSiteInfo")
    public ModelAndView editSiteInfo(SiteInfo siteInfo){
        ModelAndView mv = new ModelAndView();
        String json = JSON.toJSONString(siteInfo);
        long count = 1;
        if(stringRedisTemplate.hasKey("platform-blogs-info")){
            String info = stringRedisTemplate.opsForValue().get("platform-blogs-info");
        }
        stringRedisTemplate.opsForValue().set("platform-blogs-info",json);
        mv.setViewName("redirect:/cms/siteInfo");
        return mv;
    }








}
