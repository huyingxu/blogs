package cn.slera.blogs.controller;

import cn.slera.blogs.entity.User;
import cn.slera.blogs.vo.JsonReturnVo;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @description: 测试，临时方法
 *
 * @return: 
 * @author: Mr.hu
 * @create: 2020-04-04 14:38
 */ 
@Controller
public class TestController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     *
     * cms通用跳转路径
     * @return
     */
    @GetMapping("/cms/{url}")
    public String home(@PathVariable("url") String url){
        return "/admin/"+url;
    }
    /**
     *
     * cms通用跳转路径
     * @return
     */
    @GetMapping("/cms/{url}/{url2}")
    public String home(@PathVariable("url") String url,@PathVariable("url2") String url2){
        return "/admin/"+url+"/"+url2;
    }

    @GetMapping("/saveToken")
    @ResponseBody
    public String saveToken(HttpServletRequest request, HttpServletResponse response){
        JsonReturnVo res = new JsonReturnVo();
        String  Authorization= response.getHeader("Authorization");
        stringRedisTemplate.opsForHash().put("platform-blogs-token", request.getRequestedSessionId(), Authorization);
        res.setState("200");
        res.setResult(Authorization);
        return JSON.toJSONString(res);
    }

    @GetMapping("/getUser")
    @Cacheable(value="user-key")
    public User getUser() {
        User user = new User();
        user.setUsername("20189198");
        user.setPassword("huyi");
        System.out.println("用户对象缓存不存在，返回一个新的用户对象。");
        return user;
    }

    @GetMapping("getSiteInfo")
    public String getSiteInfo(){


        return "";
    }
    @GetMapping("/pan/index")
    public String index(){


        return "/pan/index";
    }

    /**
     *
     * cms通用跳转路径
     * @return
     */
    @PostMapping("/cms/pass")
    public String updatePassword(String oldpass,String newpass,String renewpass){




        return "/admin/";
    }


}
