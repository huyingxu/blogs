package cn.slera.blogs.controller;

import cn.slera.blogs.dto.ArticleDto;
import cn.slera.blogs.dto.ArticleWithPictureDto;
import cn.slera.blogs.entity.CategoryInfo;
import cn.slera.blogs.service.ArticleService;
import cn.slera.blogs.service.CategoryService;
import cn.slera.blogs.service.CommentService;
import cn.slera.blogs.vo.JsonReturnVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2020-04-30
 * @time: 23:07
 */
@Controller
@RequestMapping("/admin")
public class ArticleController {

    @Autowired
    ArticleService articleService;
    @Autowired
    CommentService commentService;
    @Autowired
    CategoryService categoryService;

    @GetMapping("/listArticle")
    public ModelAndView listArticle(){
        ModelAndView mv = new ModelAndView();
        List<ArticleWithPictureDto>  listArticle= articleService.listAll();
        mv.addObject("listArticle",listArticle);
        mv.setViewName("/admin/article/list");
        return mv;
    }
    @GetMapping("/addArticle")
    public ModelAndView addArticle(){
        ModelAndView mv = new ModelAndView();
        // 获取所有分类
        mv.addObject("categorys",categoryService.listAllCategory());
        mv.setViewName("/admin/article/add");
        return mv;
    }
    @GetMapping("/editArticle")
    public ModelAndView editArticle(Long id){
        ModelAndView mv = new ModelAndView();
        mv.addObject("article",articleService.getOneById(id));
        mv.addObject("categorys",categoryService.listAllCategory());
        mv.setViewName("/admin/article/edit");
        return mv;
    }
    @DeleteMapping("/delArticleCategory")@ResponseBody
    public String delArticleCategory(){

        return "success";
    }

    /**
     *  撤销文章
     *
     * @param
     * @return
     */
    @PutMapping("/revokeArticle/{id}")@ResponseBody
    public String revokeArticle(@PathVariable Long id){
        articleService.revokeArticleById(id);
        return "success";
    }

    /**
     *  发布文章
     *
     * @param
     * @return
     */
    @PutMapping("/releaseArticle/{id}")@ResponseBody
    public String releaseArticle(@PathVariable Long id){
        articleService.releaseArticle(id);
        return "success";
    }
}
