package cn.slera.blogs.controller;

import cn.slera.blogs.common.response.Result;
import cn.slera.blogs.common.response.ResultCode;
import cn.slera.blogs.dto.ArticleWithPictureDto;
import cn.slera.blogs.entity.CloudBucket;
import cn.slera.blogs.service.CloudStorageService;
import cn.slera.blogs.service.UserService;
import cn.slera.blogs.util.AliyunOSSUtil;
import cn.slera.blogs.vo.JsonReturnVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.OSS;
import com.aliyun.oss.model.Bucket;
import com.aliyun.oss.model.CnameConfiguration;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2020-04-14
 * @time: 23:25
 */
@RestController
@RequestMapping("/api")
public class ShareApiController extends BaseController{

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    CloudStorageService cloudStorageService;
    @Autowired
    private UserService userService;

    /**
     *  获取网站基础信息
     *
     * @param
     * @return
     */
    @GetMapping("/siteInfo")
    @ResponseBody
    public String siteInfo(){
        return stringRedisTemplate.opsForValue().get("platform-blogs-info");
    }

    /**
     * 获取最新的文章
     *
     * @return
     */
    @ApiOperation("分页获取文章信息")
    @GetMapping("article/list/{id}")
    @ResponseBody
    public String listLastestArticle(@PathVariable("id") Integer currentPage) {
        Integer pageSize = 12;
        // 获取总条数
        Integer  count = articleService.countArticle();
        // 求余，判断当前页参数
        Integer res = ((int)Math.ceil((double)count/(double)pageSize));
        if(currentPage > res){
            return JSON.toJSONString(new Result(ResultCode.PARAM_IS_INVALID));
        }
        // 获取文章
        List<ArticleWithPictureDto> listArticle = articleService.listArticleByPage(currentPage,pageSize);
        // 数据结果处理
        JsonReturnVo rv = new JsonReturnVo();
        rv.setState("200");
        rv.setCount(count);
        rv.setPageSize(pageSize);
        rv.setResult(listArticle);
        return JSON.toJSONString(rv);
    }

    /*------------------------------对象存储部分-----------------------------*/
    /**
     * @description: 列出所有存储空间
     *
     * @return:
     * @author: Mr.hu
     * @create: 2020/7/24 17:37
     */
    @GetMapping("/oss/listBucket")
    @ResponseBody
    public String listBucket(HttpServletRequest request, String startLogTime, String desc, String dir){
        String json;
        // 获取命名空间
        String bucket = request.getParameter("namespace");
        // 从数据库获取默认key
        CloudBucket  cloudBucket= cloudStorageService.getDefBucket();
        // 修改存储空间名
        if(bucket!=null){
            cloudBucket.setBucketName(bucket);
        }
        // 创建OSSClient实例。
        OSS ossClient = AliyunOSSUtil.OSSClientBuilder(cloudBucket);
        // 判断存储空间是否存在
        boolean exists = ossClient.doesBucketExist(cloudBucket.getBucketName());
        if(exists){
            // 列举所有存储空间。
            List<Bucket> buckets = ossClient.listBuckets();
            json = JSONObject.toJSONString(buckets);
        }else{
            Result result= new Result(ResultCode.BUCKETS_FAIL);
            json = JSONObject.toJSONString(result);
        }
        return json;
    }

    /**
     * @description: 所选空间的列表
     *
     * @return:
     * @author: Mr.hu
     * @create: 2020/7/24 17:54
     */

    @GetMapping("/oss/list")
    @ResponseBody
    public String list(HttpServletRequest request, String startLogTime, String desc, String dir,String namespace){
        ModelAndView mv = new ModelAndView();
        String downLink="";

        // 获取所有key
        CloudBucket cloudBucket= cloudStorageService.getDefBucket();

        if(namespace.equals("")){
            namespace = cloudBucket.getBucketName();
        }

        // 创建OSSClient实例。
        OSS ossClient = AliyunOSSUtil.OSSClientBuilder(cloudBucket);
        // 获取当前空间的区域
        String location = ossClient.getBucketLocation(namespace);
        // 修改地址
        cloudBucket.getCloudAdress().setEndpoint(location+".aliyuncs.com");
        // 重置OSSClient实例
        ossClient = AliyunOSSUtil.OSSClientBuilder(cloudBucket);
        // 二、获取自定义域名
        List<CnameConfiguration>  bucketUrl= ossClient.getBucketCname(namespace);
        if(bucketUrl.size()>0){
            downLink = request.getScheme()+"://"+bucketUrl.get(0).getDomain();
        }else{
            downLink = request.getScheme()+"://"+namespace+"."+cloudBucket.getCloudAdress().getEndpoint();
        }
        // 获取当前空间下文件列表
        Map map = AliyunOSSUtil.bucketFileList(ossClient,namespace,dir);

        // 关闭OSSClient。
        ossClient.shutdown();
        mv.addObject("downLink",downLink);
        mv.addObject("path",request.getRequestURI());
        mv.addObject("curUrl",dir);
        mv.addObject("bucket",namespace);
        mv.addObject("folder",map.get("newCommonPrefixes"));
        mv.addObject("file",map.get("ossObjectSummaryVo"));
        return JSONObject.toJSONString(mv.getModel());
    }
}
