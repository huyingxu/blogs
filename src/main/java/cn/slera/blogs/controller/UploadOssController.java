package cn.slera.blogs.controller;

import cn.slera.blogs.common.response.Result;
import cn.slera.blogs.common.response.ResultCode;
import cn.slera.blogs.entity.CloudAccesskey;
import cn.slera.blogs.entity.CloudAdress;
import cn.slera.blogs.entity.CloudBucket;
import cn.slera.blogs.service.CloudStorageService;
import cn.slera.blogs.util.AliyunOSSUtil;
import cn.slera.blogs.vo.JsonReturnVo;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.*;
import com.aliyun.oss.internal.OSSHeaders;
import com.aliyun.oss.model.*;
import net.minidev.json.annotate.JsonIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

@Controller
@RequestMapping("/oss")
public class UploadOssController {

    @Autowired
    CloudStorageService cloudStorageService;
    private static String FILE_URL;

    private static final Logger logger = LoggerFactory.getLogger(UploadOssController.class);

    @PreAuthorize("hasAuthority('ROLE_SUPERADMIN')")
    @GetMapping("/index")
    public ModelAndView index(){//String folders,@RequestParam(defaultValue = "slera-blogs") String cname
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/oss/index.html");
        return mv;
    }

    @GetMapping("/{bucket}/upload")
    public ModelAndView upload(HttpServletRequest request,@PathVariable(value = "bucket") String bucket,String path){
        ModelAndView mv = new ModelAndView();
        path = path.replace(request.getContextPath()+"/oss/"+bucket+"/","");
        mv.addObject("path",path);
        mv.addObject("bucket",bucket);
        mv.setViewName("/oss/upload");
        return mv;
    }

    @GetMapping("/storageList")
    public ModelAndView storageList(){
        ModelAndView mv = new ModelAndView();
        cloudStorageService.getBucketList();
        return mv;
    }

    /**
     *
     * 单文件上传至oss
     * @return
     */
    @PostMapping("/{bucket}/upload")
    @ResponseBody
    @JsonIgnore
    public JsonReturnVo upload(HttpServletRequest request,@PathVariable(value = "bucket") String bucket,@RequestParam("file[]")MultipartFile file,String path){
        JsonReturnVo result = new JsonReturnVo();
        String downLink;
        // 获取所有key
        CloudBucket  cloudBucket= cloudStorageService.getDefBucket();

        // 创建OSSClient实例。
        OSS ossClient = AliyunOSSUtil.OSSClientBuilder(cloudBucket);
        // 获取当前空间的区域
        String location = ossClient.getBucketLocation(bucket);
        // 修改地址
        cloudBucket.getCloudAdress().setEndpoint(location+".aliyuncs.com");
        // 重置OSSClient实例
        ossClient = AliyunOSSUtil.OSSClientBuilder(cloudBucket);
        List<CnameConfiguration>  bucketUrl= ossClient.getBucketCname(bucket);
        if(bucketUrl.size()>0){
            downLink = request.getScheme()+"://"+bucketUrl.get(0).getDomain();
        }else{
            downLink = request.getScheme()+"://"+bucket+"."+location+".aliyuncs.com";
        }
        try {
            File files = new File(file.getOriginalFilename());
            String fileName = file.getOriginalFilename();
            String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
            String presix = fileName.substring(0,fileName.indexOf("."));
            // MultipartFile to File
            File excelFile = File.createTempFile(presix, suffix);
            file.transferTo(excelFile);
            // 上传到oss
            String url = AliyunOSSUtil.upLoad(ossClient,bucket,excelFile,path,downLink,fileName);
            result.setState("OK");
            result.setResult("Upload complete!");
        } catch (StringIndexOutOfBoundsException e){ //下标越界
            result.setState("error");
            result.setResult("文件无后缀，禁止上传！");
            result.setErrorCode(e.toString());
        } catch (IllegalArgumentException e){ //数据异常
            result.setState("error");
            result.setResult("文件字符串太短或太长,上传失败！");
            result.setErrorCode(e.toString());
        } catch (IOException e) { // 流异常
            result.setState("error");
            result.setResult("读取数据流出现异常，上传失败");
            result.setErrorCode(e.toString());
//            e.printStackTrace();
        }
        return result;
    }

    /**
     *
     * 根据文件名删除文件
     * @return
     */
    @PostMapping("/{bucket}/removefile")
    @ResponseBody
    public Boolean removefile(@PathVariable(value = "bucket") String bucket,String file){
        // 获取所有key
        CloudBucket  cloudBucket= cloudStorageService.getDefBucket();
        // 创建OSSClient实例。
        OSS ossClient = AliyunOSSUtil.OSSClientBuilder(cloudBucket);

        return AliyunOSSUtil.delFile(ossClient,bucket,file);
    }

    /**
     *
     * 创建文件夹
     * @return
     */
    @PostMapping("/{bucket}/**/**")
    @ResponseBody
    public String createFolder(HttpServletRequest request,@PathVariable(value = "bucket") String bucket){

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
        metadata.setObjectAcl(CannedAccessControlList.Private);

        String path = request.getRequestURI().replace(request.getContextPath()+"/oss/"+bucket+"/","");
        // 获取所有key
        CloudBucket  cloudBucket= cloudStorageService.getDefBucket();
        // 创建OSSClient实例。
        OSS ossClient = AliyunOSSUtil.OSSClientBuilder(cloudBucket);
        String location = ossClient.getBucketLocation(bucket);
        cloudBucket.getCloudAdress().setEndpoint(location+".aliyuncs.com");
        ossClient = AliyunOSSUtil.OSSClientBuilder(cloudBucket);
        return AliyunOSSUtil.createFolder(ossClient,bucket,path);
    }

    //------------------后端业务---------------------

    /**
     *
     * 列出Bucket
     * @return
     */
    @GetMapping("/list")
    public ModelAndView home(){
        ModelAndView mv = new ModelAndView();
        List<CloudBucket>  cloudBucket= cloudStorageService.getBucketList();
        mv.addObject("cloudBucket",cloudBucket);
        mv.setViewName("/oss/column.html");
        return mv;
    }

    /**
     *
     * 删除Bucket
     * @return
     */
    @DeleteMapping("/delBucket")
    @ResponseBody
    public String removeBucketById(Integer bucketId){
        String result = cloudStorageService.removeBucketById(bucketId);
        if (result.equals("1")){return "success";}
        return "error";
    }

    // 添加&编辑Bucket
    @GetMapping({"/editBucket","/addBucket"})
    public ModelAndView addBucket(@RequestParam(defaultValue = "0") Integer id){
        ModelAndView mv = new ModelAndView();
        // 如果不为0 则为编辑
        if(id != 0){
            CloudBucket  bucket=cloudStorageService.editBucket(id);
            mv.addObject("bucket",bucket);
        }
        //获取 Bucket区域
        List<CloudAdress>  cloudAdressList= cloudStorageService.getCloudAdressList();
        //列出所有的秘钥
        List<CloudAccesskey> accesskeyList= cloudStorageService.getCloudAccesskeyList();
        mv.addObject("cloudAdressList",cloudAdressList);
        mv.addObject("accesskeyList",accesskeyList);
        mv.setViewName("/oss/editBucket");
        return mv;
    }
}
