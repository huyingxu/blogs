package cn.slera.blogs.controller;


import org.apache.http.client.methods.CloseableHttpResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/ssh")
public class WebSSHController {

    @GetMapping("/list")
    public ModelAndView test(){
        ModelAndView mv = new ModelAndView();

        mv.setViewName("/webssh/list");
        return mv;
    }
    @GetMapping("/index")
    public ModelAndView index(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        Cookie[] cookies = request.getCookies();
        CloseableHttpResponse res = null;
        String xsrf = "";
        if(cookies != null && cookies.length > 0){
            cookies.equals("_xsrf");
            for (Cookie cookie : cookies){
                if (cookie.getName().equals("_xsrf")) {
                    xsrf = cookie.getValue();
                }
            }
        }
        mv.addObject("xsrf",xsrf);
        mv.setViewName("/webssh/index");
        return mv;
    }



}
