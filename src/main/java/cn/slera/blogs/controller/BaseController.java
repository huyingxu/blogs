package cn.slera.blogs.controller;

import cn.slera.blogs.service.ArticleService;
import cn.slera.blogs.service.CategoryService;
import cn.slera.blogs.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 基础控制器，包含了Controller层中共有的一些Service
 *
 * @author:小胡子呀
 * @create:20120-01-02-上午 11:25
 */
public class BaseController {

    @Autowired
    ArticleService articleService;
    @Autowired
    CommentService commentService;
    @Autowired
    CategoryService categoryService;

}
