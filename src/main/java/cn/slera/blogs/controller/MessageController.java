package cn.slera.blogs.controller;

import cn.slera.blogs.entity.Message;
import cn.slera.blogs.vo.JsonReturnVo;
import cn.slera.blogs.vo.MessageVo;
import cn.slera.blogs.service.MessageService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/api")
public class MessageController {

    @Autowired
    MessageService messageService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    // Session的id
    private String SessionId;
    // 类型 1 发送短信and邮件 2 仅发送短信 3. 仅发送邮箱
    private Integer type;
    // 手机号
    private String tel;
    // 邮箱账号
    private String email;
    // 标识
    private String flag;
    // 自定义签名/邮件主题
    private String sign;
    // 邮件主题
//    private String subject;
    // 内容
    private String content;
    // 失效时间
    private Long timeout;


    /**
     *  向用户发送问候语
     **/
    @ResponseBody
    @GetMapping("/sendGreetings")
    public JsonReturnVo sendGreetings(){
        return messageService.sendGreetings();
    }

    /*
     * 根据模板ID 获取短信模板
     */
    @ResponseBody
    @GetMapping("/getSmsTemplate")
    public JsonReturnVo getWeatherInfo() {
        return messageService.getCloudMessageById("6");
    }

    /*
     * 申请短信模板(需企业认证后使用)
     */
    @ResponseBody
    @GetMapping("/addSmsTemplate")
    public JsonReturnVo addSmsTemplate() {
        Message message = new Message();
        message.setSign("短信提醒");
        message.setTemplateContent("{1}为您的登录验证码，请于{2}分钟内填写，如非本人操作，请忽略本短信。");
        message.setRemark("短信提醒");
        return messageService.addSmsTemplate(message);
    }

    @ResponseBody
    @ApiOperation("发送验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "类型 1发送所有 2发送短信 3发送邮箱", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "tel", value = "手机号", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "email", value = "邮箱地址", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "flag", value = "模板编号", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "sign", value = "自定义签名/邮件主题", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "timeout", value = "验证码有效时间", required = false, dataType = "String", paramType = "query")
    })
    @PostMapping("/sendMessage")
    public JsonReturnVo index(@RequestBody MessageVo message) {
        //获取到ServletRequestAttributes 中的session
        HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
        // 获取session 的id
        message.setSessionId(session.getId());
        JsonReturnVo result = messageService.sendMessage(message);
        return result;
    }
    @ResponseBody
    @PostMapping("/sendSms")
    @ApiOperation("发送手机验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "手机号", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "flag", value = "模板编号", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "sign", value = "自定义签名", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "timeout", value = "验证码有效时间", required = false, dataType = "String", paramType = "query")
    })
    public JsonReturnVo sendSms(@RequestBody MessageVo message) {
        //获取到ServletRequestAttributes 中的session
        HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
        // 获取session 的id
        message.setSessionId(session.getId());
        message.setType(2);
        JsonReturnVo result = messageService.sendMessage(message);
        return result;
    }
    @ResponseBody
    @PostMapping("/sendCode")
    @ApiOperation("发送手机登录验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "手机号", required = true, dataType = "String", paramType = "query"),
    })
    public JsonReturnVo sendSms(@RequestBody String tel) {
        MessageVo message = new MessageVo();
        //获取到ServletRequestAttributes 中的session
        HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
        // String 转json
        JSONObject object = JSON.parseObject(tel);
        // 获取session 的id
        message.setSessionId(session.getId());
        message.setType(2);
        message.setTel(String.valueOf(object.get("tel")));
        message.setSign("验证码登录");
        message.setUsedBy("loginCode");
        message.setTimeout(Long.valueOf(5));
        JsonReturnVo result = messageService.sendMessage(message);
        session.setAttribute("login_code",stringRedisTemplate.opsForValue().get(message.getUsedBy()+"_"+message.getSessionId()));
        return result;
    }
    @ResponseBody
    @ApiOperation("发送邮箱验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", value = "邮箱地址", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "flag", value = "模板编号", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "sign", value = "邮件主题", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "timeout", value = "验证码有效时间", required = false, dataType = "String", paramType = "query")
    })
    @PostMapping("/sendEmail")
    public JsonReturnVo sendEmail(@RequestBody MessageVo message) {
        //获取到ServletRequestAttributes 中的session
        HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
        // 获取session 的id
        message.setSessionId(session.getId());
        message.setType(3);
        JsonReturnVo result = messageService.sendMessage(message);
        return result;
    }
    @ResponseBody
    @PostMapping("/sendNotify")
    @ApiOperation("发送通知")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "手机号", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "sign", value = "自定义签名", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "stringPtrs", value = "模板参数", required = true, dataType = "String[]", paramType = "query")
    })
    public JsonReturnVo sendNotify(@RequestBody MessageVo message) {
        //获取到ServletRequestAttributes 中的session
        HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
        // 获取session 的id
        message.setSessionId(session.getId());
        message.setType(2);
        JsonReturnVo result = messageService.sendMessage(message);
        return result;
    }
}
