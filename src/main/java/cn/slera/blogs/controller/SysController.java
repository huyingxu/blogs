package cn.slera.blogs.controller;

import cn.slera.blogs.entity.CategoryInfo;
import cn.slera.blogs.entity.Comment;
import cn.slera.blogs.entity.SysLog;
import cn.slera.blogs.service.SysService;
import cn.slera.blogs.entity.SysView;
import cn.slera.blogs.vo.JsonReturnVo;
import cn.slera.blogs.vo.LayuiReturnVo;
import cn.slera.blogs.vo.SysVitalVo;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * 系统Controller
 *
 * @author:小胡子呀
 * @create:2019-06-21-上午 10:23
 */
@RestController
@RequestMapping("/admin")
public class SysController extends BaseController{


    @Autowired
    SysService sysService;

    /*
     * 首页
     */
    @GetMapping("/index")
    public ModelAndView index(HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.addObject("username",session.getAttribute("username"));
        mv.addObject("cname",session.getAttribute("cname"));
        mv.setViewName("/admin/index");
        return mv;
    }
/****************************统计页***********************************/
    /**
     * 获取统计信息
     *
     * @return
     */
    @GetMapping("/home")
    public ModelAndView home() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("vital",sysService.getVitalCount());
        mv.setViewName("/admin/home");
        return mv;
    }

    /**
     * 分页获取系统日志记录信息
     *
     * @return
     */
    @ApiOperation("分页获取SysLog信息")
    @GetMapping("/sys/log")@ResponseBody
    public String listAllLog(Integer page,Integer limit) {
        LayuiReturnVo res = new LayuiReturnVo();
        List<SysLog> log = sysService.listLogByPage(page,limit);
        res.setCode(0);
        res.setCount(sysService.listAllLog().size());
        res.setData(log);
        return JSON.toJSONString(res);
    }
    /**
     * 分页获取系统浏览记录信息
     *
     * @return
     */
    @ApiOperation("分页获取SysView信息")
    @GetMapping("/sys/view")@ResponseBody
    public String listViewByPage(Integer page,Integer limit) {
        LayuiReturnVo res = new LayuiReturnVo();
        List<SysView> view = sysService.listViewByPage(page,limit);
        res.setCode(0);
        res.setCount(sysService.listAllView().size());
        res.setData(view);
        return JSON.toJSONString(res);
    }
    /**
     * 分页获取留言
     *
     * @return
     */
    @ApiOperation("分页获取SysView信息")
    @GetMapping("/comment/list")@ResponseBody
    public String listCommentByPage(Integer page,Integer limit) {
        LayuiReturnVo res = new LayuiReturnVo();

        List<Comment> comment= commentService.listCommentByPage(page,limit);
        res.setCode(0);
        res.setCount(commentService.listAllComment().size());
        res.setData(comment);
        return JSON.toJSONString(res);
    }
/****************************分类信息***********************************/
    /*
     * 分类列表
     */
    @GetMapping("/listCategory")
    public ModelAndView getAllCategoryInfo(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+request.getContextPath();
        List<CategoryInfo>  categoryInfo= categoryService.listAllCategory();
        mv.addObject("categoryInfo",categoryInfo);
        mv.addObject("contextPath",url);
        mv.setViewName("/admin/sort/cate");
        return mv;
    }
    /*
     * 添加分类
     */
    @PostMapping("/addCategory")
    public String addCategory (CategoryInfo categoryInfo){
        categoryInfo.setNumber((byte)0);
        categoryInfo.setCreateBy(new Date());
        categoryService.addCategory(categoryInfo);
        return "redirect:/cms/sort/cate";
    }
    /*
     * 编辑分类
     */
    @PostMapping("/editCategory/{id}")
    public String updateCategoryInfo(@PathVariable Long id, @RequestBody CategoryInfo categoryInfo) {
        categoryService.updateCategory(categoryInfo);
        return "redirect:/cms/sort/cate";
    }
    /*
     * 删除分类
     */
    @DeleteMapping("/delCategory/{id}")@ResponseBody
    public String delCategoryInfo(@PathVariable Long id) {
        categoryService.deleteCategoryById(id);
        return "ok";
    }

}
