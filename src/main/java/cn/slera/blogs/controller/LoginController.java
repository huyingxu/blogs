package cn.slera.blogs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @description: 后台入口
 *
 * @return: 
 * @author: Mr.hu
 * @create: 2020-04-04 14:22
 */ 
@Controller
public class LoginController {

    /**
     * 登录入口
     *
     * @param request
     * @return
     */
    @GetMapping("/login")
    public String login(HttpServletRequest request) {
        if(request.getSession().getAttribute("Authorization") != null ){
            return "redirect:/admin/index";
        }
        return "/login";
    }

}
