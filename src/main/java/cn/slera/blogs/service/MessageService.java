package cn.slera.blogs.service;

import cn.slera.blogs.entity.Message;
import cn.slera.blogs.entity.User;
import cn.slera.blogs.vo.JsonReturnVo;
import cn.slera.blogs.dto.MessageTemplateDto;
import cn.slera.blogs.vo.MessageVo;
import cn.slera.blogs.entity.CloudAccesskey;
import cn.slera.blogs.vo.WeatherVo;

import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 *
 * 消息服务
 */
public interface MessageService {

    /**
     * 生成验证码，并发送短信
     *
     * @return
     */
    public JsonReturnVo sendMessage(MessageVo messageVo);

    /**
     * 发送问候语
     *
     * @return
     */
    public JsonReturnVo sendGreetings();

    /**
     * 发送邮件
     *
     * @param to                 目的地
     * @param subject            主题
     * @param content            内容
     * @param os                 附件
     * @param attachmentFilename 附件名
     * @param result             结果集
     * @throws Exception
     */
    public JsonReturnVo sendMail(String to, String subject, String content, ByteArrayOutputStream os, String attachmentFilename, JsonReturnVo result) throws Exception;

    /**
     * 获根据模板ID获取短信模板
     *
     * @param id 模板id
     */
    public JsonReturnVo getCloudMessageById(String id);

    /**
     * 添加短信模板
     *
     * @param
     */
    public JsonReturnVo addSmsTemplate(Message message);

    /**
     * 根据useBy 获取aaccessKey
     *
     * @param
     */
    public List<CloudAccesskey> getAccessKeyByUseBy(String useBy);

}