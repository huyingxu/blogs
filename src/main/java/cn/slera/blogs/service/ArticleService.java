package cn.slera.blogs.service;

import cn.slera.blogs.dto.ArticleDto;
import cn.slera.blogs.dto.ArticleWithPictureDto;
import cn.slera.blogs.entity.ArticlePicture;

import java.util.List;

/**
 * 文章Service
 * 说明：ArticleInfo里面封装了picture/content/category等信息
 */
public interface ArticleService {

    void addArticle(ArticleDto articleDto);

    void deleteArticleById(Long id);

    void dropArticleById(Long id);

    void updateArticle(ArticleDto articleDto);

    void updateArticleCategory(Long articleId, Long categoryId);

    ArticleDto getOneById(Long id);

    ArticlePicture getPictureByArticleId(Long id);

    List<ArticleWithPictureDto> listAll();

    List<ArticleWithPictureDto> listByCategoryId(Long id);

    List<ArticleWithPictureDto> listLastest();

    Integer countArticle();

    List<ArticleWithPictureDto> listArticleByPage(Integer currentPage,Integer pageSize);

    void revokeArticleById(Long id);

    void releaseArticle(Long id);
}
