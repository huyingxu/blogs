package cn.slera.blogs.service;

import cn.slera.blogs.entity.ExceptionLogs;

/**
 * 全局异常记录
 *
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2021-04-05
 * @time: 00:56
 */
public interface ExceptionLogsService {

    /**
     * 插入异常信息
     * @date: 2021-04-05
     * @time: 00:56
     */
    public void addExceptionLogs(ExceptionLogs logsDate);

}
