package cn.slera.blogs.service;

import cn.slera.blogs.entity.CloudAccesskey;
import cn.slera.blogs.entity.CloudAdress;
import cn.slera.blogs.entity.CloudBucket;

import java.util.List;

/**
 *
 * 云存储 OSS、COS
 */
public interface CloudStorageService {

    /**
     *
     *  列出所有 Bucket
     * @return
     */
    public List<CloudBucket> getBucketList();

    /**
     * 列出所有 仓库区域
     * @return
     */
    public List<CloudAdress>  getCloudAdressList();

    /**
     *  列出所有的秘钥
     * @return
     */
    public List<CloudAccesskey> getCloudAccesskeyList();

    /**
     *
     * 根据id删除Bucket
     * @param id
     * @return
     */
    public String removeBucketById(Integer id);

    /**
     *
     * 添加Bucket / 根据id修改Bucket
     * @param id
     * @return
     */
    public CloudBucket editBucket(Integer id);

    /**
     *
     * 添加Bucket / 根据id修改Bucket
     * @param cloudBucket
     * @return
     */
    public CloudBucket saveBucket(CloudBucket cloudBucket);

    /*
     * 列出默认Bucket
     */
    public CloudBucket getDefBucket();

}
