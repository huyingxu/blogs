package cn.slera.blogs.service;

import cn.slera.blogs.entity.User;

import java.util.List;
import java.util.Set;

public interface UserService {

    /**
     * 保存用户
     * @param user
     */
    public void save(User user);

    /**
     * 删除用户
     * @param id
     */
    public void delete(String id);

    /**
     * 查询所有用户
     * @param
     */
    public List<User> findAll();

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    User findByUsername(String username);

    /**
     * 查找用户的菜单权限标识集合
     * @param userName
     * @return
     */
    Set<String> findPermissions(String username);
}
