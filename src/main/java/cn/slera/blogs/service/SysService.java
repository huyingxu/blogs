package cn.slera.blogs.service;

import cn.slera.blogs.entity.CloudLink;
import cn.slera.blogs.entity.SysLog;
import cn.slera.blogs.entity.SysView;
import cn.slera.blogs.vo.SysVitalVo;

import java.util.List;

/**
 * 日志/访问统计等系统相关Service
 */
public interface SysService {
    void addLog(SysLog sysLog);

    void addView(SysView sysView);

    int getLogCount();

    int getViewCount();

    List<SysLog> listAllLog();

    List<SysView> listAllView();

    void transSysViewFromRedisToMysql();

    List<SysLog> listLogByPage(Integer currentPage,Integer pageSize);

    List<SysView> listViewByPage(Integer currentPage,Integer pageSize);

    SysVitalVo getVitalCount();

    CloudLink getCloudApi (String useBy);

}
