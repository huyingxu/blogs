package cn.slera.blogs.service;

import cn.slera.blogs.dto.ArticleCommentDto;
import cn.slera.blogs.entity.Comment;

import java.util.List;

/**
 * 留言的Service
 */
public interface CommentService {
    void addComment(Comment comment);

    void addArticleComment(ArticleCommentDto articleCommentDto);

    void deleteCommentById(Long id);

    void deleteArticleCommentById(Long id);

    List<Comment> listAllComment();

    List<ArticleCommentDto> listAllArticleCommentById(Long id);

    List<Comment> listCommentByPage(Integer currentPage,Integer pageSize);
}
