package cn.slera.blogs.service.impl;

import cn.slera.blogs.entity.User;
import cn.slera.blogs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 保存用户
     * @param user
     */
    public void save(User user){
        String sql = "INSERT INTO t_user (username,cname,name,password,tel,email,status) VALUES (?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql, user.getUsername(),user.getCname(),user.getName(),user.getPassword(),user.getTel(),user.getEmail(),user.getStatus());
    }

    /**
     * 删除用户
     * @param id
     */
    public void delete(String id) {
        String sql = "update t_user set status = 0 where id = ?";
        jdbcTemplate.update(sql, id);
    }

    /**
     * 查询全部用户
     * @return
     */
    public List<User> findAll() {
        String sql = "select * from t_user where status=1";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(User.class));
    }

    @Override
    public User findByUsername(String username) {
        User user = new User();
        user.setId(1L);
        user.setUsername(username);
        String password = new BCryptPasswordEncoder().encode("123");
        user.setPassword(password);
        return user;
    }

    @Override
    public Set<String> findPermissions(String username) {
        Set<String> permissions = new HashSet<>();
        permissions.add("sys:user:view");
        permissions.add("sys:user:add");
        permissions.add("sys:user:edit");
        permissions.add("sys:user:delete");
        return permissions;
    }
}
