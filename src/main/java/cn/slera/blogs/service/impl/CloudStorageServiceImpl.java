package cn.slera.blogs.service.impl;

import cn.slera.blogs.dao.CloudAccesskeyMapper;
import cn.slera.blogs.dao.CloudAdressMapper;
import cn.slera.blogs.dao.CloudBucketMapper;
import cn.slera.blogs.entity.*;
import cn.slera.blogs.service.CloudStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * 云存储 OSS、COS
 */
@Service
public class CloudStorageServiceImpl implements CloudStorageService {

    @Autowired
    CloudAccesskeyMapper cloudAccesskeyMapper;
    @Autowired
    CloudAdressMapper cloudAdressMapper;
    @Autowired
    CloudBucketMapper cloudBucketMapper;


    /**
     *
     *  列出所有 Bucket
     * @return
     */
    public List<CloudBucket> getBucketList(){
        CloudBucketExample cloudBucketExample = new CloudBucketExample();
        cloudBucketExample.or().andStatusEqualTo("1");
        cloudBucketExample.or().andStatusEqualTo("2");
        List<CloudBucket> cloudBucket= cloudBucketMapper.selectByExample(cloudBucketExample);
        return cloudBucket;
    }
    /**
     * 列出所有 Bucket区域
     * @return
     */
    public List<CloudAdress>  getCloudAdressList(){
        return cloudAdressMapper.selectByExample(new CloudAdressExample());
    }

    /**
     *  列出所有 秘钥
     * @return
     */
    public List<CloudAccesskey> getCloudAccesskeyList(){
        return cloudAccesskeyMapper.selectByExample(new CloudAccesskeyExample());
    }
    /**
     *
     * 根据id删除Bucket
     * @param id
     * @return
     */
    public String removeBucketById(Integer id){
        CloudBucket cloudBucket = new CloudBucket();
        cloudBucket.setStatus("0");
        cloudBucket.setId(id);
        int result= cloudBucketMapper.removeBucketById(cloudBucket);
        return String.valueOf(result);
    }
    /**
     *
     * 根据id查询Bucket
     * @param id
     * @return
     */
    public CloudBucket editBucket(Integer id){
        CloudBucketExample example= new CloudBucketExample();
        example.or().andIdEqualTo(id);
        CloudBucket bucket=cloudBucketMapper.selectByExample(example).get(0);
        return bucket;
    }

    /**
     *
     * 添加Bucket / 根据id修改Bucket
     * @param cloudBucket
     * @return
     */
    public CloudBucket saveBucket(CloudBucket cloudBucket){

        return  null;
    }

    /**
     *
     *  列出默认Bucket
     * @return
     */
    public CloudBucket getDefBucket(){
        CloudBucketExample cloudBucketExample = new CloudBucketExample();
        cloudBucketExample.or().andStatusEqualTo("2");
        List<CloudBucket> cloudBucket= cloudBucketMapper.selectByExample(cloudBucketExample);
        return cloudBucket.size() > 0 ?cloudBucket.get(0):null;
    }
}
