package cn.slera.blogs.service.impl;

import cn.slera.blogs.config.MailSender;
import cn.slera.blogs.dao.CloudAccesskeyMapper;
import cn.slera.blogs.dao.CommonMapper;
import cn.slera.blogs.dao.EmailConfigMapper;
import cn.slera.blogs.dao.MessageVarMapper;
import cn.slera.blogs.entity.*;
import cn.slera.blogs.service.SysService;
import cn.slera.blogs.service.UserService;
import cn.slera.blogs.vo.JsonReturnVo;
import cn.slera.blogs.dto.MessageTemplateDto;
import cn.slera.blogs.vo.MessageVo;
import cn.slera.blogs.service.MessageService;
import cn.slera.blogs.util.CodeUtil;
import cn.slera.blogs.vo.WeatherVo;
import com.alibaba.fastjson.JSON;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20210111.models.SendSmsResponse;
import com.tencentcloudapi.sms.v20210111.models.SendStatus;
import com.tencentcloudapi.sms.v20210111.models.AddSmsTemplateRequest;
import com.tencentcloudapi.sms.v20210111.models.AddSmsTemplateResponse;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.activation.DataHandler;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class MessageServiceImpl implements MessageService {

    @Autowired
    CloudAccesskeyMapper cloudAccesskeyMapper;
    @Autowired
    CommonMapper commonMapper;
    @Autowired
    EmailConfigMapper emailConfigMapper;
    @Autowired
    MessageVarMapper messageVarMapper;
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    MessageService messageService;
    @Autowired
    SysService sysService;
    @Autowired
    UserService userService;

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private MailSender mailSender;

    /**
     *  生成验证码，并调用短信、邮箱接口
     **/
    public JsonReturnVo sendMessage(MessageVo messageVO){
        JsonReturnVo result = new JsonReturnVo();
        CodeUtil CodeUtil=new CodeUtil();
        List<MessageTemplateDto> list;
        // redis 键名
        String key = messageVO.getUsedBy()+"_"+messageVO.getSessionId();
        // 校验验证码有效期 (防止恶意提交)
        Boolean codeExpiration= this.codeExpiration(key,messageVO.getTimeout());
        if(codeExpiration == false){
            result.setResult("Operation is too frequent!");
            return result;
        }
        //生成验证码
        String code= CodeUtil.getNonce_str();
        //验证码存入redis,并设置时效性
        stringRedisTemplate.opsForValue().set(key, code,messageVO.getTimeout()*60, TimeUnit.SECONDS);

        // 查询短信模板。flag 默认为default
        try {
            list= commonMapper.cloud_messageByuseBy(messageVO.getUseBy());
        } catch (Exception e) {
            result.setState("error");
            result.setResult(e);
            return result;
        }
        if (list.size() == 0){
            result.setState("error");
            result.setResult("The secret key doesn't seem to be found !");
            return result;
        }

        // 处理短信变量
        String [] templateParam={code,Long.toString(messageVO.getTimeout())};

        // 发送信息 type 1发送全部 2发送短信 3发送邮箱
        if (list.size() != 0 && (messageVO.getType() == 1 || messageVO.getType() == 2)){
            //从数据库中取出信息接口的信息
            MessageTemplateDto messageTemplateVo = list.get(0);
            // 自定义签名
            if(messageVO.getSign() !=null && !messageVO.getSign().equals("")){
                messageTemplateVo.setSign(messageVO.getSign());
            }

            // 号码转换数组
            messageVO.setPhones(("+86"+ messageVO.getTel().replace(",",",+86")).split(","));
            //发送验证码
            result = this.sendSms(messageTemplateVo,messageVO);
            // 异常 return
            if(result.getState().equals("error")){ return result;}
//                return result;
        }
        //发送邮件   type 1发送全部 2发送短信 3发送邮箱
        if (list.size() != 0 && (messageVO.getType() == 1 || messageVO.getType() == 3)){
            MessageTemplateDto messageTemplate= list.get(0);
            // 验证码和有效时间存入模板中
            messageVO.setContent(messageTemplate.getTemplate_content().replace("{1}",code).replace("{2}",messageVO.getTimeout().toString()));
            try {
                result = this.sendMail(messageVO.getEmail(),messageVO.getSign(),messageVO.getContent(),null,null,result);
            } catch (Exception e) {
                result.setState("error");
                result.setResult(e);
            }
        }
        return result;
    }

    /**
     *  向用户发送问候语
     **/
    public JsonReturnVo sendGreetings(){
        JsonReturnVo result = new JsonReturnVo();
        CodeUtil CodeUtil=new CodeUtil();
        List<MessageTemplateDto> messageList;

        MessageVo message = new MessageVo();
        message.setUseBy("weather");
        message.setType(2);

        // 获取短信模板
        try {
            messageList = commonMapper.cloud_messageByuseBy(message.getUseBy());
        } catch (Exception e) {
            result.setErrorCode(e.getMessage());
            return result;
        }
        if (messageList.size() == 0){
            result.setResult("The secret key doesn't seem to be found !");
            return result;
        }
        //从数据库中取出信息接口的信息
        MessageTemplateDto messageTemplate = messageList.get(0);

        // 获取天气接口key
        List<CloudAccesskey> weatherKey= messageService.getAccessKeyByUseBy("weather");
        if(weatherKey.size() == 0){
            result.setErrorCode("Weather Key does not exist,Please refer to the documentation（ https://tianqiapi.com/index/doc ）Get key");
            return result;
        }
        // 天气api key信息
        String appid=weatherKey.get(0).getAccesskeyId();
        String appsecret=weatherKey.get(0).getAccesskeySecret();
        // 获取天气API链接
        CloudLink link = sysService.getCloudApi("weather");

        // 查找所有用户名
        List<User> users = userService.findAll();
        // 短信发送结果
        Object[] smsResult=new Object[users.size()];
        // for循环索引
        Integer i = 0;
        // 根据用户不同的地区获取地域天气
        for (User user:users){
            // 获取key
            String api=link.getLink()+"?unescape=1&version=v6&appid="+appid+"&appsecret="+appsecret+"&cityid="+user.getSignature();
            // 获取天气信息
            WeatherVo weather= JSON.parseObject(restTemplate.getForObject(api,String.class), WeatherVo.class);

            // 获取年月日
            Calendar cal = Calendar.getInstance();
            int day = cal.get(Calendar.DATE);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);


            MessageVarExample example = new MessageVarExample();
            example.or().andTemplateIdEqualTo(messageTemplate.getId());
            List<MessageVar> varList= messageVarMapper.selectByExample(example);

            if (varList.size() == 0){
                result.setErrorCode("Failed to get the variable. Please confirm whether the variable exists in the message");
                return result;
            }

            // 处理短信变量
            String[] a = new String[varList.size()+1];
            // 昵称
            a[0] = user.getName();

            for (MessageVar var:varList){
                // 变量名
                String varName=var.getVarName();
                //排序
                Integer id=var.getVarSort();
                // 处理数组
                if(var.getVarSort()==0){
                    a[0] = varName;
                }else if(varName.equals("year")){ // 年
                    a[id] = String.valueOf(year);
                }else if(varName.equals("month")){ //月
                    a[id] = String.valueOf(month);
                }else if(varName.equals("day")){ //日
                    a[id] = String.valueOf(day);
                }else if(varName.equals("tem")){ // 当前温度
                    a[id] = weather.getTem();
                }else if(varName.equals("tem1")){ //最高温度
                    a[id] = weather.getTem1();
                }else if(varName.equals("tem2")){ //最低温度
                    a[id] = weather.getTem2();
                }else if(varName.equals("city")){ // 城市名
                    a[id] = weather.getCity();
                }else if(varName.equals("wea")){ // 天气状况
                    a[id] = weather.getWea();
                }else if(varName.equals("waeek")) { // 星期
                    a[id] = weather.getWeek();
                }else if(varName.equals("date")){ // 时间
                    a[id] = new SimpleDateFormat("HH:mm").format(new Date());
                }else {
                    a[id]=varName;
                }
            }

            // 手机号
            message.setTel(user.getTel());
            // 存入消息对象
            message.setStringPtrs(a);
            // 号码转换数组
            message.setPhones(("+86"+ message.getTel().replace(",",",+86")).split(","));
            // 发送短信
            smsResult[i]= this.sendSms(messageTemplate,message).getResult();
        }

        // 处理返回结果
        result.setState("success");
        result.setCount(smsResult.length);
        result.setResult(smsResult);
        return result;
    }

    /**
     *  判断验证码的有效期 (防止恶意提交)
     * @param key
     * @param timeout
     * @return
     */
    public Boolean codeExpiration(String key,Long timeout){
        Long time=stringRedisTemplate.getExpire(key != null ? key: "");
        //一、用户第一次收到请求，执行该方法
        //二、再次收到用户请求且时间超过1分钟不超过设定的超时时间,则发送验证码
        if ((timeout -1 )*60 > time){
            //如果该用户再次发送请然后求根据key删除缓存
            stringRedisTemplate.delete(key != null ? key: "");
            return true;
        }else {
            return false;
        }
    }

    /**
     *  根据模板ID获取短信模板
     **/
    public JsonReturnVo getCloudMessageById(String id){
        JsonReturnVo result = new JsonReturnVo();
        CodeUtil CodeUtil=new CodeUtil();
        List<MessageTemplateDto> list;

        try {
            list= commonMapper.cloud_messageById(Long.valueOf(id));
        } catch (Exception e) {
            result.setState("error");
            result.setResult(e);
            return result;
        }

        if (list.size() == 0){
            result.setState("error");
            result.setResult("The secret key doesn't seem to be found !");
            return result;
        }
        // 不返回key信息
        list.get(0).setCloudAccesskey(null);
        //输出
        result.setState("success");
        result.setResult(list.get(0));
        return result;
    }

    // 发送短信
    public JsonReturnVo
    sendSms(MessageTemplateDto messageTemplate,MessageVo message){
        JsonReturnVo result = new JsonReturnVo();

        Credential cred = new Credential(messageTemplate.getCloudAccesskey().getAccesskeyId(),messageTemplate.getCloudAccesskey().getAccesskeySecret());
        // 实例化要请求产品(以cvm为例)的client对象
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setSignMethod(ClientProfile.SIGN_TC3_256);
        SmsClient smsClient = new SmsClient(cred, messageTemplate.getRegion());
        SendSmsRequest sendSmsRequest = new SendSmsRequest();
        sendSmsRequest.setSmsSdkAppId(messageTemplate.getAppId());
        sendSmsRequest.setPhoneNumberSet(message.getPhones());
        sendSmsRequest.setTemplateId(messageTemplate.getTemplateId());
        sendSmsRequest.setTemplateParamSet(message.getStringPtrs());
        sendSmsRequest.setSignName(messageTemplate.getSign());
        try {
            SendSmsResponse sendSmsResponse= smsClient.SendSms(sendSmsRequest); //发送短信
            for (SendStatus a :sendSmsResponse.getSendStatusSet()){
                System.out.println(a.getPhoneNumber()+"----"+a.getMessage()+"----"+a.getCode());
            }
            result.setResult(sendSmsResponse.getSendStatusSet());
            result.setCount(sendSmsResponse.getSendStatusSet().length);
            result.setState("success");
        } catch (TencentCloudSDKException e) {
            result.setResult(e.getMessage());
            return result;
        }
        return result;
    }

    // 发送邮件
    @Override
    public JsonReturnVo sendMail(String to, String subject, String content, ByteArrayOutputStream os, String attachmentFilename, JsonReturnVo result) throws Exception {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            EmailConfigExample example = new EmailConfigExample();
            example.or().andStatusEqualTo("2");
            List<EmailConfig> emailList= emailConfigMapper.selectByExample(example);
            EmailConfig emailConfig;
            if(emailList.size() > 0 ){
                emailConfig = emailList.get(0);
            }else{
                result.setState("error");
                result.setResult("Configuration information for email not found!");
                return result;
            }
            mailSender.changeDataSource(emailConfig,result);
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(emailConfig.getUsername());
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content);
            helper.setText(content,"text/html;charset=UTF-8");
            message.setDataHandler(new DataHandler(content, "text/html;charset=UTF-8"));
            if (null != os) {
                //附件
                InputStreamSource inputStream = new ByteArrayResource(os.toByteArray());
                helper.addAttachment(attachmentFilename, inputStream);
            }
            mailSender.send(message);
            result.setState("success");
        } catch (Exception e) {
            result.setState("error");
            result.setResult(e);
        }
        return result;
    }

    // 添加短信模板
    public JsonReturnVo addSmsTemplate(Message message) {
        JsonReturnVo result = new JsonReturnVo();
        List<CloudAccesskey> list;

        try {
            CloudAccesskeyExample example = new CloudAccesskeyExample();
            example.or().andUsebyEqualTo("tencentyun");
            list = cloudAccesskeyMapper.selectByExample(example);
        } catch (Exception e) {
            result.setState("error");
            result.setResult(e);
            return result;
        }
        if (list.size() == 0){
            result.setState("error");
            result.setResult("The secret key doesn't seem to be found !");
            return result;
        }

        //从数据库中取出信息接口的信息
        CloudAccesskey cloudAccesskey = list.get(0);

        Credential cred = new Credential(cloudAccesskey.getAccesskeyId(),cloudAccesskey.getAccesskeySecret());
        // 实例化要请求产品(以cvm为例)的client对象
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setSignMethod(ClientProfile.SIGN_TC3_256);
        SmsClient smsClient = new SmsClient(cred, "ap-beijing");
        AddSmsTemplateRequest req = new AddSmsTemplateRequest();

        // 模板名称
        req.setTemplateName(message.getSign());
        // 模板内容
        req.setTemplateContent(message.getTemplateContent());
        // 短信类型：0表示普通短信, 1表示营销短信
        req.setSmsType(Long.valueOf(0));
        // 0：国内短信，1：国际/港澳台短信
        req.setInternational(Long.valueOf(0));
        // 模板备注：例如申请原因，使用场景等
        req.setRemark(message.getRemark());
        try {
            /* 通过 client 对象调用 AddSmsTemplate 方法发起请求。注意请求方法名与请求对象是对应的
             * 返回的 res 是一个 AddSmsTemplateResponse 类的实例，与请求对象对应 */
            AddSmsTemplateResponse res = smsClient.AddSmsTemplate(req);
            // 输出 JSON 格式的字符串回包
            System.out.println(AddSmsTemplateResponse.toJsonString(res));
            // 可以取出单个值，您可以通过官网接口文档或跳转到 response 对象的定义处查看返回字段的定义
            System.out.println(res.getRequestId());
            result.setResult("success");
            result.setResult(res.toJsonString(res));
        }catch (TencentCloudSDKException e){
            result.setState("error");
            result.setErrorCode(e.toString());
        }

        return result;
    }

    /**
     * 根据useBy 获取aaccessKey
     *
     * @param
     */
    public List<CloudAccesskey> getAccessKeyByUseBy(String useBy){
        CloudAccesskeyExample example = new CloudAccesskeyExample();
        example.or().andUsebyEqualTo(useBy);
        return cloudAccesskeyMapper.selectByExample(example);
    }

}
