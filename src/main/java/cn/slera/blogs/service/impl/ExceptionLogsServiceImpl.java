package cn.slera.blogs.service.impl;

import cn.slera.blogs.dao.ExceptionLogsMapper;
import cn.slera.blogs.entity.ExceptionLogs;
import cn.slera.blogs.service.ExceptionLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 全局异常记录
 *
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2021-04-05
 * @time: 00:56
 */
@Component
public class ExceptionLogsServiceImpl implements ExceptionLogsService {

    @Autowired
    ExceptionLogsMapper exceptionLogsMapper;

    /**
     * 插入异常信息
     * @date: 2021-04-05
     * @time: 00:56
     */
    public void addExceptionLogs(ExceptionLogs logsDate) {
        exceptionLogsMapper.insert(logsDate);
    }
}
