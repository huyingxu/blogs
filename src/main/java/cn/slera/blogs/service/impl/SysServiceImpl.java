package cn.slera.blogs.service.impl;

import cn.slera.blogs.entity.*;
import cn.slera.blogs.service.SysService;
import cn.slera.blogs.dao.SysLogMapper;
import cn.slera.blogs.dao.SysViewMapper;
import cn.slera.blogs.dao.CloudLinkMapper;
import cn.slera.blogs.vo.SysVitalVo;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * SysService实现类
 *
 * @author:slera
 * @create:2019-06-20-下午 20:17
 */
@Component
public class SysServiceImpl implements SysService {

    @Autowired
    SysLogMapper sysLogMapper;
    @Autowired
    SysViewMapper sysViewMapper;
    @Autowired
    CloudLinkMapper cloudLinkMapper;

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

//    private static SysServiceImpl sysService;
//
//    @PostConstruct
//    public void init() {
//        sysService = this;
//        sysService.sysLogMapper = this.sysLogMapper;
//        sysService.sysViewMapper = this.sysViewMapper;
//    }


    /**
     * 增加一条日志信息
     *
     * @param sysLog
     */
    @Override
    public void addLog(SysLog sysLog) {
        sysLogMapper.insertSelective(sysLog);
    }

    /**
     * 增加一条访问量
     *
     * @param sysView
     */
    @Override
    public void addView(SysView sysView) {
        sysViewMapper.insertSelective(sysView);
    }

    /**
     * 获取日志的总数量
     *
     * @return 日志的总数量
     */
    @Override
    public int getLogCount() {
        SysLogExample example = new SysLogExample();
        return sysLogMapper.selectByExample(example).size();
    }

    /**
     * 返回当前网站的访问量
     *
     * @return
     */
    @Override
    public int getViewCount() {
        SysViewExample example = new SysViewExample();
        return sysViewMapper.selectByExample(example).size();
    }

    /**
     * 返回所有的日志信息
     *
     * @return
     */
    @Override
    public List<SysLog> listAllLog() {
        SysLogExample example = new SysLogExample();
        example.setOrderByClause("id asc");
        return sysLogMapper.selectByExample(example);
    }

    /*
     *  分页获取日志信息
     */
    public List<SysLog> listLogByPage(Integer currentPage,Integer pageSize){
        PageHelper.startPage(currentPage, pageSize);
        SysLogExample example = new SysLogExample();
        example.setOrderByClause("create_by desc");
        return sysLogMapper.selectByExample(example);
    }
    /**
     * 返回所有的访问信息
     *
     * @return
     */
    @Override
    public List<SysView> listAllView() {
        List<String> s= stringRedisTemplate.opsForList().range("platform-blogs-session-view",0,-1);
        List<SysView> sv = new ArrayList<>();
        for(String String:s){
            sv.add(JSONObject.parseObject(String , SysView.class));
        }
//        SysViewExample example = new SysViewExample();
//        example.setOrderByClause("id desc");
        return sv;
    }

    public List<SysView> listViewByPage(Integer currentPage,Integer pageSize){
        PageHelper.startPage(currentPage, pageSize);
        Integer staPage= ((currentPage - 1) * pageSize)+1;
        List<String> s= stringRedisTemplate.opsForList().range("platform-blogs-session-view",staPage,staPage == 1 ? pageSize : staPage+pageSize);
        List<SysView> sv = new ArrayList<>();
        for(String String:s){
            sv.add(JSONObject.parseObject(String , SysView.class));
        }
        return sv;
    }
    /*
     * 获取统计信息
     */
    public SysVitalVo getVitalCount(){
        SysVitalVo sv = new SysVitalVo();
        Long viewCount = stringRedisTemplate.opsForHash().size("platform-blogs-session");
        Long totalView= stringRedisTemplate.opsForList().size("platform-blogs-session-view");
        sv.setTodayView(viewCount);
        sv.setTotalView(totalView);
        sv.setTodayVisits(Long.valueOf(146));
        sv.setTotalVisits(Long.valueOf(854));
        sv.setWeekLog(Long.valueOf(145));
        sv.setTotalLog(Long.valueOf(964));
        sv.setWeekComment(Long.valueOf(10));
        sv.setTotalComment(Long.valueOf(124));

        return sv;
    }

    /**
     * 定时任务，转移至mysql
     *
     * @return
     */
    @Override
    public void transSysViewFromRedisToMysql(){
        try {
            // 从platform-blogs-session查询未转移到mysql的总条数，并作为platform-blogs-session-view的索引
            Long count = stringRedisTemplate.opsForHash().size("platform-blogs-session");
            if (count >= 1) {
                // 查询当天的数据
                List<String> s = stringRedisTemplate.opsForList().range("platform-blogs-session-view", 0, count - 1);
                for (String String : s) {
                    addView(JSONObject.parseObject(String, SysView.class));
                }
                // 执行完毕清除platform-blogs-session中的记录
                stringRedisTemplate.delete("platform-blogs-session");//根据key删除缓存
            }
        }catch (Exception e){}
    }

    //  获取云api链接
    public CloudLink getCloudApi (String useBy){
        CloudLinkExample example = new CloudLinkExample();
        example.or().andUsebyEqualTo(useBy);
        List<CloudLink> cloudLinks = cloudLinkMapper.selectByExample(example);
        if (cloudLinks.size() != 0){
            return cloudLinks.get(0);
        }
        return null;
    }
}
