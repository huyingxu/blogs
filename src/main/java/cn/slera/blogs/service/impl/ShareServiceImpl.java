package cn.slera.blogs.service.impl;

import cn.slera.blogs.dao.AuthorityMapper;
import cn.slera.blogs.dao.UserAuthorityMapper;
import cn.slera.blogs.dao.UserMapper;
import cn.slera.blogs.entity.*;
import cn.slera.blogs.entity.view.ViewUserAuthority;
import cn.slera.blogs.service.ShareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ShareServiceImpl implements ShareService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AuthorityMapper authorityMapper;
    @Autowired
    private UserAuthorityMapper userAuthorityMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;

    /*
     *
     * 登录
     * @return
     * */
    public User login(User user){
        UserExample userExample = new UserExample();
        userExample.or().andUsernameEqualTo(user.getUsername());
        userExample.or().andPasswordEqualTo(user.getPassword());
        List<User> users= userMapper.selectByExample(userExample);
        //
        if(users !=null) {
            return users.get(0);
        }
        return  null;
    }
    /**
     * @description: 根据username查找用户
     *
     * @return:
     * @author: Mr.hu
     * @create: 2020-03-21 17:06
     */
    public User findByUsername(String username) {
        UserExample userExample = new UserExample();
        userExample.or().andUsernameEqualTo(username);
        List<User> users= userMapper.selectByExample(userExample);
        if(users.size()!= 0) {
            User user = users.get(0);
            user.setUsername(users.get(0).getUsername());
            String password = new BCryptPasswordEncoder().encode(user.getPassword());
            user.setPassword(password);
            return user;
        }
        return  null;
    }
    /**
     * @description: 查询该用户所有权限
     *
     * @return:
     * @author: Mr.hu
     * @create: 2020-03-21 17:09
     */
    public List<ViewUserAuthority> findAuthorityByUsername(String username){
//        // 查询所有权限
//        AuthorityExample example1 = new AuthorityExample();
//        List <Authority> authoritys=authorityMapper.selectByExample(example1);
//        // 查询用户权限
//        UserAuthorityExample example = new UserAuthorityExample();
//        example.or().andUsernameEqualTo(username);
//        List<UserAuthority> userAuthorities = userAuthorityMapper.selectByExample(example);

        String sql = "select * from view_user_authority where username = \'"+username+"\'";
        return jdbcTemplate.query(sql,new BeanPropertyRowMapper(ViewUserAuthority.class));
    }

    public User findByPhone(String phone){
        UserExample userExample = new UserExample();
        userExample.or().andTelEqualTo(phone);
        List<User> users= userMapper.selectByExample(userExample);
        if(users.size()!= 0) {
            User user = users.get(0);
            user.setUsername(users.get(0).getUsername());
            String password = new BCryptPasswordEncoder().encode(user.getPassword());
            user.setPassword(password);
            return user;
        }
        return  null;
    }
}
