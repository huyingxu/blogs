package cn.slera.blogs.service.impl;

import cn.slera.blogs.dao.*;
import cn.slera.blogs.entity.*;
import cn.slera.blogs.service.ArticleService;
import cn.slera.blogs.dao.*;
import cn.slera.blogs.entity.*;
import cn.slera.blogs.dao.*;
import cn.slera.blogs.dto.ArticleDto;
import cn.slera.blogs.dto.ArticleWithPictureDto;
import cn.slera.blogs.entity.*;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 文章Service实现类
 * 说明：ArticleInfo里面封装了picture/content/category等信息
 *
 * @author:slera
 * @create:20120-01-02-上午 9:29
 */
@Service
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	ArticleInfoMapper articleInfoMapper;
	@Autowired
	ArticlePictureMapper articlePictureMapper;
	@Autowired
	ArticleCategoryMapper articleCategoryMapper;
	@Autowired
	ArticleContentMapper articleContentMapper;
	@Autowired
	CategoryInfoMapper categoryInfoMapper;
	@Autowired
	CommonMapper commonMapper;

	private static byte MAX_LASTEST_ARTICLE_COUNT = 5;

	/**
	 * 增加一篇文章信息
	 * 说明：需要对应的写入t_article_picture/t_article_content/t_article_category表
	 * 注意：使用的是Article封装类
	 *
	 * @param articleDto 文章封装类
	 */
	@Override
	public void addArticle(ArticleDto articleDto) {
		// 获取最后一篇文章id
		Long articleId = articleDto.getId()!=null? articleDto.getId():getArticleLastestId()+1;

		// 增加文章信息表 - title/summary
		ArticleInfo articleInfo = new ArticleInfo();
		articleInfo.setId(articleId);
		articleInfo.setIsTop(articleDto.getTop());
		articleInfo.setTitle(articleDto.getTitle());
		articleInfo.setSummary(articleDto.getSummary());
		articleInfo.setCreateBy(articleDto.getCreateBy());
		articleInfo.setModifiedBy(articleDto.getCreateBy());

		// 增加文章题图信息 - pictureUrl/articleId
		ArticlePicture articlePicture = new ArticlePicture();
		articlePicture.setPictureUrl(articleDto.getPictureUrl());
		articlePicture.setArticleId(articleId);
		articlePicture.setCreateBy(articleDto.getCreateBy());
		articlePicture.setModifiedBy(articleDto.getCreateBy());

		// 增加文章内容信息表 - content/articleId
		ArticleContent articleContent = new ArticleContent();
		articleContent.setId(articleId);
		articleContent.setContent(articleDto.getContent());
		articleContent.setArticleId(articleId);
		articleContent.setCreateBy(articleDto.getCreateBy());
		articleContent.setModifieldBy(articleDto.getCreateBy());

		ArticleInfoExample exam = new ArticleInfoExample();
		exam.or().andTitleEqualTo(articleDto.getTitle());
		exam.or().andSummaryEqualTo(articleDto.getSummary());
		List<ArticleInfo> newarticleInfo = articleInfoMapper.selectByExample(exam);

		// 分类信息保存
		String[] categoryNames= articleDto.getCategoryNames().split(",");
		for(String category:categoryNames){
			Long articleInfoId;
			// 分类为空、多余，的情况下跳过
			if(category.equals("")){ continue;}
			CategoryInfoExample example = new CategoryInfoExample();
			example.or().andNameEqualTo(category);
			List<CategoryInfo> listCategoryInfo= categoryInfoMapper.selectByExample(example);
			// 如果分类数据库不存在, 则插入新分类
			if(listCategoryInfo.size()==0){
				CategoryInfo categoryInfo = new CategoryInfo();
				categoryInfo.setName(category);
				categoryInfo.setNumber((byte)1);
				categoryInfo.setId(articleId);
				categoryInfo.setCreateBy(articleDto.getCreateBy());
				categoryInfo.setModifiedBy(articleDto.getCreateBy());
				categoryInfoMapper.insertSelective(categoryInfo);
				// 获取 分类id
				CategoryInfoExample example2 = new CategoryInfoExample();
				example2.or().andNameEqualTo(category);
				List<CategoryInfo> listCategoryInfo2= categoryInfoMapper.selectByExample(example2);
				articleInfoId = listCategoryInfo2.get(0).getId();
			}else{
				// 对应文章的数量要加1
				CategoryInfo categoryInfo = listCategoryInfo.get(0);
				categoryInfo.setNumber((byte) (categoryInfo.getNumber() + 1));
//				categoryInfo.setCreateBy(articleDto.getCreateBy());
				categoryInfo.setModifiedBy(articleDto.getCreateBy());
				categoryInfoMapper.updateByPrimaryKeySelective(categoryInfo);
				articleInfoId = categoryInfo.getId();
			}
			// 增加文章分类信息表 - articleId/categoryId
			ArticleCategory articleCategory = new ArticleCategory();
			articleCategory.setArticleId(articleId);
			articleCategory.setCategoryId(articleInfoId);
			articleCategory.setCreateBy(articleDto.getCreateBy());
			articleCategory.setModifiedBy(articleDto.getCreateBy());
			articleCategoryMapper.insertSelective(articleCategory);
		}
		if(articleDto.getId()!=null){
			articleInfo.setId(articleId);
			articleInfoMapper.updateByPrimaryKeySelective(articleInfo);
			articlePictureMapper.updateByPrimaryKeySelective(articlePicture);
			articleContentMapper.updateByPrimaryKeySelective(articleContent);
		}else{
			articleInfoMapper.insertSelective(articleInfo);
			articlePictureMapper.insertSelective(articlePicture);
			articleContentMapper.insertSelective(articleContent);

		}
	}

	/**
	 * 删除一篇文章
	 * 说明：假删除
	 *
	 * @param id
	 */
	@Override
	public void deleteArticleById(Long id) {
		articleInfoMapper.updateIsUsedByPrimaryKey(Long.valueOf(2),id);
	}

	/**
	 * 永久删除一篇文章
	 * 说明：需要对应删除t_article_picture/t_article_content/t_article_category表中的内容
	 *
	 * @param id
	 */
	@Override
	public void dropArticleById(Long id) {
		// 获取对应的文章信息
		ArticleDto articleDto = getOneById(id);

		CategoryInfoExample example = new CategoryInfoExample();
		example.or().andIdEqualTo(id);
		List<CategoryInfo> listCategoryInfo= categoryInfoMapper.selectByExample(example);
		// 文章对应标签的数量要减1
		for(CategoryInfo categoryInfo:listCategoryInfo){
			CategoryInfo newCategoryinfo = categoryInfo;
			newCategoryinfo.setNumber((byte) (categoryInfo.getNumber()-1));
			categoryInfoMapper.updateByPrimaryKeySelective(newCategoryinfo);
		}
		// 删除文章信息中的数据
		articleInfoMapper.deleteByPrimaryKey(articleDto.getId());
		// 删除文章题图信息数据
		articlePictureMapper.deleteByPrimaryKey(articleDto.getArticlePictureId());
		// 删除文章内容信息表
		articleContentMapper.deleteByPrimaryKey(articleDto.getArticleContentId());
		// 删除文章分类信息表
		articleCategoryMapper.deleteByPrimaryKey(articleDto.getArticleCategoryId());
	}

	/**
	 * 更改文章的分类信息
	 *
	 * @param articleId
	 * @param categoryId
	 */
	@Override
	public void updateArticleCategory(Long articleId, Long categoryId) {
		ArticleCategoryExample example = new ArticleCategoryExample();
		example.or().andArticleIdEqualTo(articleId);
		ArticleCategory articleCategory = articleCategoryMapper.selectByExample(example).get(0);
		// 对应改变分类下的文章数目
		CategoryInfo categoryInfo = categoryInfoMapper.selectByPrimaryKey(articleCategory.getCategoryId());
		categoryInfo.setNumber((byte) (categoryInfo.getNumber() - 1));
		categoryInfoMapper.updateByPrimaryKeySelective(categoryInfo);
		categoryInfo = categoryInfoMapper.selectByPrimaryKey(categoryId);
		categoryInfo.setNumber((byte) (categoryInfo.getNumber() + 1));
		categoryInfoMapper.updateByPrimaryKeySelective(categoryInfo);
		// 更新t_article_category表字段
		articleCategory.setCategoryId(categoryId);
		articleCategoryMapper.updateByPrimaryKeySelective(articleCategory);
	}

	/**
	 * 更新文章信息
	 * 说明：需要对应更改t_article_picture/t_article_content/t_article_category表中的内容
	 * 注意：我们使用的是封装好的Article文章信息类
	 *
	 * @param articleDto 自己封装的Article信息类
	 */
	@Override
	public void updateArticle(ArticleDto articleDto) {
		// 更新文章信息中的数据
		ArticleInfo articleInfo = new ArticleInfo();
		articleInfo.setId(articleDto.getId());
		articleInfo.setTitle(articleDto.getTitle());
		articleInfo.setSummary(articleDto.getSummary());
		articleInfo.setIsTop(articleDto.getTop());
		articleInfo.setTraffic(articleDto.getTraffic());
		articleInfoMapper.updateByPrimaryKeySelective(articleInfo);
		// 更新文章题图信息数据
		ArticlePictureExample pictureExample = new ArticlePictureExample();
		pictureExample.or().andArticleIdEqualTo(articleDto.getId());
		ArticlePicture articlePicture = articlePictureMapper.selectByExample(pictureExample).get(0);
//        articlePicture.setId(articleDto.getArticlePictureId());
		articlePicture.setPictureUrl(articleDto.getPictureUrl());
		articlePictureMapper.updateByPrimaryKeySelective(articlePicture);
		// 更新文章内容信息数据
		ArticleContentExample contentExample = new ArticleContentExample();
		contentExample.or().andArticleIdEqualTo(articleDto.getId());
		ArticleContent articleContent = articleContentMapper.selectByExample(contentExample).get(0);
//		articleContent.setArticleId(articleDto.getId());
//		articleContent.setId(articleDto.getArticleContentId());
		articleContent.setContent(articleDto.getContent());
		articleContentMapper.updateByPrimaryKeySelective(articleContent);
		// 更新文章分类信息表
		ArticleCategoryExample categoryExample = new ArticleCategoryExample();
		categoryExample.or().andArticleIdEqualTo(articleDto.getId());
		ArticleCategory articleCategory = articleCategoryMapper.selectByExample(categoryExample).get(0);
//        articleCategory.setId(articleDto.getArticleCategoryId());
		articleCategory.setCategoryId(articleDto.getCategoryId());
		articleCategoryMapper.updateByPrimaryKeySelective(articleCategory);
	}

	/**
	 * 获取一篇文章内容
	 * 说明：需要对应从t_article_picture/t_article_content/t_article_category表中获取内容
	 * 并封装好
	 *
	 * @param id 文章ID
	 * @return 填充好数据的ArticleInfo
	 */
	@Override
	public ArticleDto getOneById(Long id) {
		ArticleDto articleDto = new ArticleDto();
		// 填充文章基础信息
		ArticleInfo articleInfo = articleInfoMapper.selectByPrimaryKey(id);
		articleDto.setId(articleInfo.getId());
		articleDto.setTitle(articleInfo.getTitle());
		articleDto.setSummary(articleInfo.getSummary());
		articleDto.setTop(articleInfo.getIsTop());
		articleDto.setCreateBy(articleInfo.getCreateBy());
		// 文章访问量要加1
		articleInfo.setTraffic(articleInfo.getTraffic() + 1);
		articleDto.setTraffic(articleInfo.getTraffic());
		articleInfoMapper.updateByPrimaryKey(articleInfo);
		// 填充文章内容信息
		ArticleContentExample example = new ArticleContentExample();
		example.or().andArticleIdEqualTo(id);
		List<ArticleContent> articleContentList = articleContentMapper.selectByExample(example);
		if(!articleContentList.isEmpty()){
			articleDto.setContent(articleContentList.get(0).getContent());
			articleDto.setArticleContentId(articleContentList.get(0).getId());
		}
		// 填充文章题图信息
		ArticlePictureExample example1 = new ArticlePictureExample();
		example1.or().andArticleIdEqualTo(id);
		List<ArticlePicture> articlePictureList = articlePictureMapper.selectByExample(example1);
		if(!articlePictureList.isEmpty()){
			articleDto.setPictureUrl(articlePictureList.get(0).getPictureUrl());
			articleDto.setArticlePictureId(articlePictureList.get(0).getId());
		}

		// 填充文章分类信息
		ArticleCategoryExample example2 = new ArticleCategoryExample();
		example2.or().andArticleIdEqualTo(id);
		List<ArticleCategory> listArticleCategory = articleCategoryMapper.selectByExample(example2);
        articleDto.setArticleCategory(listArticleCategory);
//		articleDto.setArticleCategoryId(articleCategory.getId());
        // 填充文章分类基础信息
		List<CategoryInfo> listCategoryInfo = new ArrayList<>();
        for(ArticleCategory list:listArticleCategory){
            CategoryInfoExample example3 = new CategoryInfoExample();
            example3.or().andIdEqualTo(list.getCategoryId());
			List<CategoryInfo> categoryInfoList = categoryInfoMapper.selectByExample(example3);
			if(!categoryInfoList.isEmpty()){
				CategoryInfo categoryInfo = categoryInfoList.get(0);
				listCategoryInfo.add(categoryInfo);
				articleDto.setCategoryNames(articleDto.getCategoryNames()==null? categoryInfo.getName():articleDto.getCategoryNames()+ ","+categoryInfo.getName());
			}
        }
		articleDto.setCategoryInfo(listCategoryInfo);
		return articleDto;
	}

	/**
	 * 获取所有的文章内容
	 *
	 * @return 封装好的Article集合
	 */
	@Override
	public List<ArticleWithPictureDto> listAll() {
		// 1.先获取所有的数据
		List<ArticleWithPictureDto> articles = listAllArticleWithPicture();
		// 2.然后再对集合进行重排，置顶的文章在前
		LinkedList<ArticleWithPictureDto> list = new LinkedList<>();
		for (int i = 0; i < articles.size(); i++) {
			if (true == articles.get(i).getTop()) {
				list.addFirst(articles.get(i));
			} else {
				list.addLast(articles.get(i));
			}
		}
		articles = new ArrayList<>(list);

		return articles;
	}

	/**
	 * 通过分类id返回该分类下的所有文章
	 *
	 * @param id 分类ID
	 * @return 对应分类ID下的所有文章(带题图)
	 */
	@Override
	public List<ArticleWithPictureDto> listByCategoryId(Long id) {
		ArticleCategoryExample example = new ArticleCategoryExample();
		example.or().andCategoryIdEqualTo(id);
		List<ArticleCategory> articleCategories = articleCategoryMapper.selectByExample(example);
		List<ArticleWithPictureDto> articles = new ArrayList<>();
		for (int i = 0; i < articleCategories.size(); i++) {
			Long articleId = articleCategories.get(i).getArticleId();
			ArticleWithPictureDto articleWithPictureDto = new ArticleWithPictureDto();
			// 填充文章基础信息
			ArticleInfoExample example1 = new ArticleInfoExample();
			example1.or().andIdEqualTo(articleId);
			List<ArticleInfo> articleInfoList = articleInfoMapper.selectByExample(example1);
			if(!articleInfoList.isEmpty()){
				articleWithPictureDto.setId(articleInfoList.get(0).getId());
				articleWithPictureDto.setTitle(articleInfoList.get(0).getTitle());
				articleWithPictureDto.setSummary(articleInfoList.get(0).getSummary());
				articleWithPictureDto.setTop(articleInfoList.get(0).getIsTop());
				articleWithPictureDto.setTraffic(articleInfoList.get(0).getTraffic());
				// 填充文章图片信息
				ArticlePictureExample example2 = new ArticlePictureExample();
				example2.or().andArticleIdEqualTo(articleInfoList.get(0).getId());
				List<ArticlePicture> articlePictureList = articlePictureMapper.selectByExample(example2);
				if(!articlePictureList.isEmpty()){
					articleWithPictureDto.setArticlePictureId(articlePictureList.get(0).getId());
					articleWithPictureDto.setPictureUrl(articlePictureList.get(0).getPictureUrl());
				}
			}
			articles.add(articleWithPictureDto);
		}


		// 对集合进行重排，置顶的文章在前
		LinkedList<ArticleWithPictureDto> list = new LinkedList<>();
		for (int i = 0; i < articles.size(); i++) {
			if (true == articles.get(i).getTop()) {
				list.add(articles.get(i));
			} else {
				list.addLast(articles.get(i));
			}
		}
		articles = new ArrayList<>(list);

		return articles;
	}

	/**
	 * 获取最新的文章信息
	 *
	 * @return 返回五篇最新的文章数据
	 */
	@Override
	public List<ArticleWithPictureDto> listLastest() {
		return listAllArticleWithPicture();
	}

	/**
	 * 返回最新插入一条数据的ID
	 *
	 * @return
	 */
	private Long getArticleLastestId() {
		ArticleInfoExample example = new ArticleInfoExample();
		example.setOrderByClause("id desc");
		return articleInfoMapper.selectByExample(example).get(0).getId();
	}

	/**
	 * 通过文章ID获取对应的文章题图信息
	 *
	 * @param id 文章ID
	 * @return 文章ID对应的文章题图信息
	 */
	@Override
	public ArticlePicture getPictureByArticleId(Long id) {
		ArticlePictureExample example = new ArticlePictureExample();
		example.or().andArticleIdEqualTo(id);
		return articlePictureMapper.selectByExample(example).isEmpty() ? null:articlePictureMapper.selectByExample(example).get(0);
	}

	/**
	 * 获取所有的文章信息（带题图）
	 *
	 * @return
	 */
	private List<ArticleWithPictureDto> listAllArticleWithPicture() {
		ArticleInfoExample example = new ArticleInfoExample();
		//获取已发布和待发布的文章
		example.or().andIsUsedEqualTo(1);
		example.or().andIsUsedEqualTo(0);
		example.setOrderByClause("id desc");
		// 无添加查询即返回所有
		List<ArticleInfo> articleInfos = articleInfoMapper.selectByExample(example);
		List<ArticleWithPictureDto> articles = new ArrayList<>();
		for (ArticleInfo articleInfo : articleInfos) {
			ArticleWithPictureDto articleWithPictureDto = new ArticleWithPictureDto();
			// 填充文章基础信息
			articleWithPictureDto.setId(articleInfo.getId());
			articleWithPictureDto.setTitle(articleInfo.getTitle());
			articleWithPictureDto.setSummary(articleInfo.getSummary());
			articleWithPictureDto.setTop(articleInfo.getIsTop());
			articleWithPictureDto.setTraffic(articleInfo.getTraffic());
			articleWithPictureDto.setCreate_by(articleInfo.getCreateBy());
			articleWithPictureDto.setIsUsed(articleInfo.getIsUsed());
			// 填充文章题图信息
			ArticlePictureExample example1 = new ArticlePictureExample();
			example1.or().andArticleIdEqualTo(articleInfo.getId());
			List<ArticlePicture> articlePictureList = articlePictureMapper.selectByExample(example1);
			if(!articlePictureList.isEmpty()){
				articleWithPictureDto.setArticlePictureId(articlePictureList.get(0).getId());
				articleWithPictureDto.setPictureUrl(articlePictureList.get(0).getPictureUrl());
			}
			articles.add(articleWithPictureDto);
		}
		return articles;
	}

	public Integer countArticle(){
		return commonMapper.countArticle();
	}

	/*************************************************/
	/**
	 * 获取分页获取文章
	 *
	 * @return
	 */
	public List<ArticleWithPictureDto> listArticleByPage(Integer currentPage,Integer pageSize){
		//设置分页信息，分别是当前页数和每页显示的总记录数【记住：必须在mapper接口中的方法执行之前设置该分页信息】
		PageHelper.startPage(currentPage, pageSize);
		ArticleInfoExample example = new ArticleInfoExample();
		//获取已发布和待发布的文章
		example.or().andIsUsedEqualTo(1);
		example.or().andIsUsedEqualTo(0);
		example.setOrderByClause("id desc");
		// 无添加查询即返回所有
		List<ArticleInfo> articleInfos = articleInfoMapper.selectByExample(example);
		List<ArticleWithPictureDto> articles = new ArrayList<>();
		for (ArticleInfo articleInfo : articleInfos) {
			ArticleWithPictureDto articleWithPictureDto = new ArticleWithPictureDto();
			// 填充文章基础信息
			articleWithPictureDto.setId(articleInfo.getId());
			articleWithPictureDto.setTitle(articleInfo.getTitle());
			articleWithPictureDto.setSummary(articleInfo.getSummary());
			articleWithPictureDto.setTop(articleInfo.getIsTop());
			articleWithPictureDto.setTraffic(articleInfo.getTraffic());
			articleWithPictureDto.setCreate_by(articleInfo.getCreateBy());
			// 填充文章题图信息
			ArticlePictureExample example1 = new ArticlePictureExample();
			example1.or().andArticleIdEqualTo(articleInfo.getId());
			List<ArticlePicture> articlePictureList = articlePictureMapper.selectByExample(example1);
			if(!articlePictureList.isEmpty()){
				articleWithPictureDto.setArticlePictureId(articlePictureList.get(0).getId());
				articleWithPictureDto.setPictureUrl(articlePictureList.get(0).getPictureUrl());
			}
			articles.add(articleWithPictureDto);
		}
		return articles;
	}

	/**
	 *  撤销文章
	 *
	 * @param id  文章id
	 * @return
	 */
	public void revokeArticleById(Long id){
		articleInfoMapper.updateIsUsedByPrimaryKey(Long.valueOf(0),id);

	}

	/**
	 *  发布文章
	 *
	 * @param id  文章id
	 * @return
	 */
	public void releaseArticle(Long id){
		articleInfoMapper.updateIsUsedByPrimaryKey(Long.valueOf(1),id);
	}

}
