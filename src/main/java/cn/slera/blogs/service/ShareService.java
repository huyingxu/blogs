package cn.slera.blogs.service;

import cn.slera.blogs.entity.User;
import cn.slera.blogs.entity.view.ViewUserAuthority;

import java.util.List;
import java.util.Set;

public interface ShareService {


    /*
    *
    * 登录
     * @return
    * */
    public User login(User user);

    /**
     * @description: 根据username查找用户
     *
     * @return:
     * @author: Mr.hu
     * @create: 2020-03-21 17:06
     */
    public User findByUsername(String username);
    /**
     * @description: 查询该用户所有权限
     *
     * @return: 
     * @author: Mr.hu
     * @create: 2020-03-21 17:09
     */ 
    public List<ViewUserAuthority> findAuthorityByUsername(String username);

    public User findByPhone(String phone);

}
