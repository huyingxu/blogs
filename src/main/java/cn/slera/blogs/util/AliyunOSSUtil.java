package cn.slera.blogs.util;

import cn.slera.blogs.entity.CloudBucket;
import cn.slera.blogs.vo.OSSObjectSummaryVo;
import com.aliyun.oss.*;
import com.aliyun.oss.model.*;
import jdk.nashorn.internal.runtime.regexp.RegExp;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

@Component
public class AliyunOSSUtil {

    private static final Logger logger = LoggerFactory.getLogger(AliyunOSSUtil.class);
    private static String FILE_URL;

    /**
     * 所选存储桶下文件列表
     *
     * @param
     * @return 如果上
     */
    public static Map bucketFileList(OSS ossClient,String bucket,String path){
        final int maxKeys = 1000;
        String nextMarker = null;
        ObjectListing objectListing;
        Map map = new HashMap();

        ListObjectsRequest listObjectsRequest = new ListObjectsRequest(bucket);
        listObjectsRequest.setDelimiter("/");
        if (!path.equals("/")) {
            listObjectsRequest.setPrefix(path);
        }else if(path.equals("/")){ path = ""; }

        // 三、获取当前桶(Bucket)下文件列表
        do {
            objectListing = ossClient.listObjects(listObjectsRequest.withMarker(nextMarker).withMaxKeys(maxKeys));

            List<OSSObjectSummary> sums = objectListing.getObjectSummaries();
            nextMarker = objectListing.getNextMarker();
        } while (objectListing.isTruncated());

        List<OSSObjectSummaryVo> ossObjectSummaryVo = new ArrayList<>();
        //  处理文件
        for (OSSObjectSummary ojs:objectListing.getObjectSummaries()){
            OSSObjectSummaryVo objectSummaryVo = new OSSObjectSummaryVo();
            if(!ojs.getKey().endsWith("/")){
                objectSummaryVo.setKey(ojs.getKey().replace(path,""));
                objectSummaryVo.setSize(PrintSizeUtil.getPrintSize((ojs.getSize())));
                ossObjectSummaryVo.add(objectSummaryVo);
            }
        }
        // 处理文件夹
        List<String> newCommonPrefixes = new ArrayList<>();
        for(String cp:objectListing.getCommonPrefixes()){
            newCommonPrefixes.add(cp.replaceFirst(path,""));
        }

        map.put("newCommonPrefixes",newCommonPrefixes);
        map.put("ossObjectSummaryVo",ossObjectSummaryVo);
        return map;
    }


    /**
     * 上传文件。
     *
     * @param file 需要上传的文件路径
     * @return 如果上传的文件是图片的话，会返回图片的"URL"，如果非图片的话会返回"非图片，不可预览。文件路径为：+文件路径"
     */
    public static String upLoad(OSS ossClient, String bucketName, File file, String filePath, String fileHost, String fileName) {

        // 默认值为：true
        boolean isImage = true;
        // 判断所要上传的图片是否是图片，图片可以预览，其他文件不提供通过URL预览
        try {
            Image image = ImageIO.read(file);
            isImage = image == null ? false : true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("------OSS文件上传开始--------" + fileName);

        // 文件名格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
        // String dateString = sdf.format(new Date()) + "." + suffix; // 20190322010634.jpg

        // 判断文件
        if (file == null) {
            return null;
        }

        try {
            // 判断容器是否存在,不存在就创建
            if (!ossClient.doesBucketExist(bucketName)) {
                ossClient.createBucket(bucketName);
                CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
                createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
                ossClient.createBucket(createBucketRequest);
            }
            // 设置文件路径和名称
//            String fileUrl = fileHost + "/" + (dateString + "/" + UUID.randomUUID().toString().replace("-", "") + "-" + file.getName());
            String fileUrl = fileName;
            if(filePath != null){
                fileUrl = filePath + fileUrl;
            }

            if (isImage) {//如果是图片，则图片的URL为：....
                FILE_URL = fileHost+"/"+fileUrl;
            } else {
                FILE_URL = fileHost+"/"+fileUrl;
                logger.info("非图片,不可预览。文件路径为：" + fileUrl);
            }

            // 上传文件
            PutObjectResult result = ossClient.putObject(new PutObjectRequest(bucketName, fileUrl, file));
            // 设置权限(公开读)
            ossClient.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);
            if (result != null) {
                logger.info("------OSS文件上传成功------" + fileHost+"/"+fileUrl);
            }
        } catch (OSSException oe) {
            logger.error(oe.getMessage());
        } catch (ClientException ce) {
            logger.error(ce.getErrorMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return FILE_URL;
    }

    /**
     * 通过文件名下载文件
     *
     * @param objectName    要下载的文件名
     * @param localFileName 本地要创建的文件名
     */
    public static void downloadFile(OSS ossClient,String bucketName,String objectName, String localFileName) {

        // 下载OSS文件到本地文件。如果指定的本地文件存在会覆盖，不存在则新建。
        ossClient.getObject(new GetObjectRequest(bucketName, objectName), new File(localFileName));
        // 关闭OSSClient。
        ossClient.shutdown();
    }

    /**
     * 删除文件
     * objectName key 地址
     *
     * @param filePath
     */
    public static Boolean delFile(OSS ossClient,String bucketName,String filePath) {
        logger.info("删除开始，objectName=" + filePath);

        // 删除Object.
        boolean exist = ossClient.doesObjectExist(bucketName, filePath);
        if (!exist) {
            logger.error("文件不存在,filePath={}", filePath);
            return false;
        }
        logger.info("删除文件,filePath={}", filePath);
        ossClient.deleteObject(bucketName, filePath);
        ossClient.shutdown();
        return true;
    }

    /**
     * 批量删除
     *
     * @param keys
     */
    public static Boolean delFileList(OSS ossClient,String bucketName,List<String> keys) {
        try {
            // 删除文件。
            DeleteObjectsResult deleteObjectsResult = ossClient.deleteObjects(new DeleteObjectsRequest(bucketName).withKeys(keys));
            List<String> deletedObjects = deleteObjectsResult.getDeletedObjects();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            ossClient.shutdown();
        }
        return true;

    }

    /**
     * 获取文件夹
     *
     * @param filePath 文件路径
     * @return
     */
    public static List<String> fileFolder(OSS ossClient,String bucketName,String filePath) {
        // 构造ListObjectsRequest请求。
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest(bucketName);
        // 设置正斜线（/）为文件夹的分隔符。
        listObjectsRequest.setDelimiter("/");
        // 设置prefix参数来获取fun目录下的所有文件。
        if (StringUtils.isNotBlank(filePath)) {
            listObjectsRequest.setPrefix(filePath + "/");
        }
        // 列出文件
        ObjectListing listing = ossClient.listObjects(listObjectsRequest);
        // 遍历所有commonPrefix
        List<String> list = new ArrayList<>();
        for (String commonPrefix : listing.getCommonPrefixes()) {
            String newCommonPrefix = commonPrefix.substring(0, commonPrefix.length() - 1);
            list.add(newCommonPrefix);
        }
        // 关闭OSSClient
        ossClient.shutdown();
        return list;
    }

    /**
     * 根据路径获取目下下文件夹、文件
     *
     * @param filePath 文件路径
     * @return
     */
    public static ObjectListing findContent(OSS ossClient,String bucketName,String filePath,String cname) {
        // 构造ListObjectsRequest请求。
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest(cname);
        // 设置正斜线（/）为文件夹的分隔符。
        listObjectsRequest.setDelimiter("/");
        // 设置prefix参数来获取fun目录下的所有文件。
        if (StringUtils.isNotBlank(filePath)) {
            listObjectsRequest.setPrefix(filePath + "/");
        }
        // 列出文件
        ObjectListing listing = ossClient.listObjects(listObjectsRequest);
        // 关闭OSSClient
        ossClient.shutdown();
        return listing;
    }

    /**
     * 获得url链接
     *
     * @param objectName
     * @return
     */
    public static String getUrl(OSS ossClient,String bucketName,String objectName) {
        // 设置权限(公开读)
        ossClient.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);
        // 设置图片处理样式。
//        String style = "image/resize,m_fixed,w_100,h_100/rotate,90";
        Date expiration = new Date(new Date().getTime() + 3600l * 1000 * 24 * 365 * 100);
        GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, objectName, HttpMethod.GET);
        req.setExpiration(expiration);
//        req.setProcess(style);
        URL signedUrl = ossClient.generatePresignedUrl(req);
        // 关闭OSSClient。
        logger.info("------OSS文件文件信息--------" + signedUrl.toString());
        ossClient.shutdown();
        if (signedUrl != null) {
            return signedUrl.toString();
        }
        return null;
    }

    // 获取文 MultipartFile 文件后缀名工具
    public static String getSuffix(MultipartFile fileupload) {
        String originalFilename = fileupload.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
        System.out.println(suffix);
        return suffix;
    }

    /**
     * 创建文件夹
     *
     * @param folder
     * @return
     */
    public static String createFolder(OSS ossClient,String bucketName,String folder) {
        // 文件夹名
        final String keySuffixWithSlash = folder;
        // 判断文件夹是否存在，不存在则创建
        if (!ossClient.doesObjectExist(bucketName, keySuffixWithSlash)) {
            // 创建文件夹
            ossClient.putObject(bucketName, keySuffixWithSlash, new ByteArrayInputStream(new byte[0]));
            logger.info("创建文件夹成功");
            // 得到文件夹名
            OSSObject object = ossClient.getObject(bucketName, keySuffixWithSlash);
            String fileDir = object.getKey();
            ossClient.shutdown();
            return fileDir;
        }
        return keySuffixWithSlash;
    }

    /**
     * @description: 创建OSSClient实例
     *
     * @return:
     * @author: Mr.hu
     * @create: 2020/7/24 17:44
     */
    public static OSS OSSClientBuilder(CloudBucket cloudBucket){
        return new OSSClientBuilder().build(cloudBucket.getCloudAdress().getEndpoint(),cloudBucket.getCloudAccesskey().getAccesskeyId(), cloudBucket.getCloudAccesskey().getAccesskeySecret());
    }
}
