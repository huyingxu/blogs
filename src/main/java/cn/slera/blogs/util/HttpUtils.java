package cn.slera.blogs.util;

import cn.slera.blogs.vo.HttpResult;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * HTTP工具类
 * @author Louis
 * @date Jun 29, 2019
 */
public class HttpUtils {

	/**
	 * 获取HttpServletRequest对象
	 * @return
	 */
	public static HttpServletRequest getHttpServletRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}

	/**
	 * 处理Result错误代码,将其输出信息到浏览器
	 * @param response
	 * @param data 结果
	 * @throws IOException
	 */
	public static void write(HttpServletResponse response,Object data){
		try {
			response.setContentType("application/json; charset=utf-8");
			String json = JSONObject.toJSONString(data);
			response.getWriter().print(json);
			response.getWriter().flush();
			response.getWriter().close();
		}catch (IOException e){

		}

	}
	
	/**
	 * 自定义错误代码,将其输出信息到浏览器
	 * @param response
	 * @param code
	 * @param data 结果
	 * @throws IOException
	 */
	public static void write(HttpServletResponse response,int code,Object data) throws IOException {
		response.setContentType("application/json; charset=utf-8");
        HttpResult result = HttpResult.ok(code,data);
        String json = JSONObject.toJSONString(result);
        response.getWriter().print(json);
        response.getWriter().flush();
        response.getWriter().close();

	}
	
}
