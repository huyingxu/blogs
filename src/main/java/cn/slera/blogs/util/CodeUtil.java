package cn.slera.blogs.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;

import java.util.Random;
import java.security.SecureRandom;

/**
 * 作用：用于发送短信验证码
 *
 */
@Controller
public class CodeUtil {

    // 字符串
    // private static final String SYMBOLS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String SYMBOLS = "0123456789"; // 数字

    private static final Random RANDOM = new SecureRandom();

    @Autowired
    StringRedisTemplate stringRedisTemplate;

     /**
     * 获取长度为 6 的随机数字
     * @return 随机数字
     */
     public static String getNonce_str() {
     // 如果需要4位，那 new char[4] 即可，其他位数同理可得
        char[] nonceChars = new char[6];
        for (int index = 0; index < nonceChars.length; ++index) {
           nonceChars[index] = SYMBOLS.charAt(RANDOM.nextInt(SYMBOLS.length()));
        }
     return new String(nonceChars);
     }
}
