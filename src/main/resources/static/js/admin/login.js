/**
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2020-03-24
 * @time: 21:01
 */

// 初始化变量
var user_Boolean = false;
var password_Boolean = false;
var varconfirm_Boolean = false;
var emaile_Boolean = false;
var Mobile_Boolean = false;
var code_Boolean = false;

// 表单校验函数
// username
function reg_user(){
    user_message ="";
    if ((/^[a-z0-9_-]{4,8}$/).test($(".reg_user").val())){
        $('.user_hint').html("✔").css("color","green");
        user_Boolean = true;
    }else {
        // $("#t_user").css("border","0.5px solid red"); //表单红框框
        $('.user_hint').html("×").css("color","red");
        user_Boolean = false;
    }
};

// password
function reg_password(){
    if ((/^[a-z0-9_-]{4,16}$/).test($(".reg_password").val())){
        $('.password_hint').html("✔").css("color","green");
        password_Boolean = true;
    }else{
        $('.password_hint').html("×").css("color","red");
        password_Boolean = false;
    }
};

// password_confirm
function reg_confirm(){
    if (($(".reg_password").val())==($(".reg_confirm").val())){
        $('.confirm_hint').html("✔").css("color","green");
        varconfirm_Boolean = true;
    }else {
        $('.confirm_hint').html("×").css("color","red");
        varconfirm_Boolean = false;
    }
};

// Email
function reg_email(){
    if ((/^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/).test($(".reg_email").val())){
        $('.email_hint').html("✔").css("color","green");
        emaile_Boolean = true;
    }else {
        $('.email_hint').html("×").css("color","red");
        emaile_Boolean = false;
    }
};

// Mobile
function reg_mobile(){
    if ((/^1[34578]\d{9}$/).test($(".reg_mobile").val())){
        $('.mobile_hint').html("✔").css("color","green");
        Mobile_Boolean = true;
    }else {
        $('.mobile_hint').html("×").css("color","red");
        Mobile_Boolean = false;
    }
};

// smsCode
function reg_code(){
    if ((/^[0-9_-]{6}$/).test($(".reg_code").val())){
        $('.code_hint').html("✔").css("color","green");
        code_Boolean = true;
    }else {
        // $("#t_user").css("border","0.5px solid red");
        $('.code_hint').html("×").css("color","red");
        code_Boolean = false;
    }
};

// click 触发校验
function submit(){
    if(user_Boolean && password_Boolean || Mobile_Boolean == true && code_Boolean == true){
        login();
    }else {
        var type = $('#type').val();
        if(type != 2){
          // username
          reg_user();
          // password
          reg_password();
        }else {
          // Mobile
          reg_mobile();
          // code
          reg_code();
        }
    }
};

//通用存入Cookie
function addCookie(objName, objValue, objHours) {
    var str = objName + "=" + escape(objValue); //编码
    if (objHours > 0) {//为0时不设定过期时间，浏览器关闭时cookie自动消失
        var date = new Date();
        var ms = objHours * 3600 * 1000;
        date.setTime(date.getTime() + ms);
        str += "; expires=" + date.toGMTString();
    }
    document.cookie = str;
}

//写入身份令牌
function addAuthory(objValue) {
    var str = "Authorization=" + escape(objValue.data.token); //编码
    var date = new Date();
    var ms = 12 * 3600 * 1000;
    date.setTime(date.getTime() + ms);
    str += "; expires=" + date.toGMTString()+";path=/";
    document.cookie = str;
    return objValue;
}

//修改cookie的值
function editCookie(name,value,expiresHours){
    var cookieString=name+"="+escape(value);
    if(expiresHours>0){
        var date=new Date();
        date.setTime(date.getTime()+expiresHours*1000); //单位是毫秒
        cookieString=cookieString+";expires=" + date.toGMTString();
    }
    document.cookie=cookieString;
}

//根据名字获取cookie的值
function getCookieValue(name){
    var strCookie=document.cookie;
    var arrCookie=strCookie.split("; ");
    for(var i=0;i<arrCookie.length;i++){
        var arr=arrCookie[i].split("=");
        if(arr[0]==name){
            return unescape(arr[1]);
            break;
        }else{
            return "";
            break;
        }
    }

}

/**
 * 从Cookies中获取token
 *  */
function getToken() {
    var strcookie = document.cookie;//获取cookie字符串
    var arrcookie = strcookie.split("; ");//分割
    //遍历匹配
    for (var i = 0; i < arrcookie.length; i++) {
        var arr = arrcookie[i].split("=");
        if (arr[0] == "token") {
            return arr[1];
        }
    }
    return "";
}

function switchSms() {
    $('#txt1').hide();
    $('#txt2').show();
    $('#userLogin').empty();
    $('#type').val("2");
    $('#smsLogin').append('<div class="control-group"><div class="controls"><label for="tel" class="fa fa-mobile fa-2x" style="font-size: 1.6em; margin-top: -2px; margin-left: 2px;"></label><input id="tel" name="tel" placeholder="手机号" tabindex="1"  onBlur="reg_mobile()" class="reg_mobile form-control input-medium" style="width: 95%;"><span class="mobile_hint"></span></div></div><div class="control-group"><div class="controls"> <label for="t_code" class="fa fa-key fa-fw"></label> <input id="t_code" type="text" name="t_code" placeholder="验证码" onBlur="reg_code()" tabindex="2" class="reg_code form-control input-medium" style="width: 64%;">&ensp; <input type="button" id="code" class="btn btn-primary"style="padding: 2.5%;width: 28.8%;background-color: #D4F2FC;border-color: #8FBBF7;" onclick="settime(this)" value="获取验证码"/><span class="code_hint"></span></div></div>');
    // 获取当前时间搓
    var getTime = Math.round(new Date().getTime()/1000);
    // 从cookie获取上次存入的时间戳
    var valTime = getCookieValue("secondsremained");//获取cookie值
    // 获取差值
    var result = getTime - valTime;
    // 发送验证码
    if ( 60 > result) {
        var countdown = 60 - result;
        var tim = setInterval(function() {
            countdown--;
            $("#code").attr("disabled", true);
            $("#code").removeAttr("width");
            $("#code").val(countdown + "s后重试");
            if(countdown <= 0 ) {
                clearInterval(tim);
                $("#code").removeAttr("disabled");
                $("#code").css("width","28.8%");
                $("#code").val(" 重新发送 ");
            }
        }, 1000) //每1000毫秒执行一次
    }
}

function switchUser() {
    $("#txt1").css("display","block");
    $("#txt2").css("display","none");
    $('#type').val("1");
    // $('#userLogin').show();
    $('#userLogin').append('<div class="control-group"><div class="controls"><label for="t_user" class="fa fa-user"></label><input type="text" id="t_user" name="t_user" onBlur="reg_user()" tabindex="1" placeholder="用户名、手机号" class="form-control input-medium reg_user" style="width: 95%;" ><span class="user_hint"></span></div></div><div class="control-group"><div class="controls"><label for="t_pwd" class="control-label fa fa-asterisk"></label><input type="password" id="t_pwd"  onBlur="reg_password()" name="password" tabindex="2"  placeholder="请输入4-16位密码" class="form-control input-medium reg_password" style="width: 95%;"><span class="password_hint"></span></div></div>');
    $('#smsLogin').empty();
}

function settime(obj) {
    // 手机号校验
    reg_mobile();
    if(Mobile_Boolean){
        // 获取当前时间戳
        var getTime = Math.round(new Date().getTime()/1000);
        var countdown = 60;
        sendCode();
        var tim = setInterval(function() {
            countdown--;
            $("#code").removeAttr("width");
            obj.setAttribute("disabled", true);
            obj.value= countdown + "s后重试";
            if(countdown <= 0 ) {
                clearInterval(tim);
                $(obj).removeAttr("disabled");
                $(obj).css("width","28.8%");
                obj.value="重新发送";
            }

        }, 1000) //每1000毫秒执行一次
        addCookie("secondsremained", getTime, 60); //添加至cookie,有效时间60s
    }
}

function sendCode(){
    var tel = $("#tel").val();
    $.ajax({
        type: "post",
        url: "api/sendCode",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: '{\"tel\":\"'+tel+'\"}',
        success: function(json) {
            console.log(json);
        },
        error: function(a) {
            console.log("不可知异常！");
        }
    });
}

function login(){
    var login_type= $("#type").val();
    if(login_type == 1){
        var username = $("#t_user").val();
        var password = $("#t_pwd").val();
        if(password != undefined){
            password = b64(password);
        }
        var data = {username: username, password: password,login_type:login_type}
    }else if(login_type == 2){
        var phone = $("#tel").val();
        var code = $("#t_code").val();
        var data = {phone: phone, code:code,login_type:login_type}
    }
    $.ajax({
        type: "post",
        data: JSON.stringify(data),
        url: "",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        success: function(json) {
            if(json.code == 200){
                addAuthory(json);
                window.location.href="admin/index";
            }else{
                console.log(json);alert("错误代码："+json.data.code+",原因："+json.data.message);
            }
        },
        error: function () {
            alert("登录失败了！");
        }
    });


}

