/**
 * @email xiaohuzi@slera.cn
 * @author: Mr.hu
 * @date: 2020-04-06
 * @time: 22:36
 */

// 获取当前页面地址
var url = window.location.origin;

var query = window.location.href;

//发布文章，保存草稿箱
function saveArticle(){
    var title = t_title.value;
    var summary = t_summary.value;
    var pictureUrl = t_pictureUrl.value;
    var categoryName = t_categoryName.value;
    var articleTop = document.getElementById("articleTop").checked;
    var articleId = document.getElementById("articleId").value;
    var content = localStorage.getItem('vditorarticle'+articleId);


    // 不为空才能增加
    var comment = {
        id: articleId != "" ? articleId :"",
        title: title,
        summary: summary,
        pictureUrl: pictureUrl,
        categoryNames: categoryName,
        articleTop: articleTop,
        content: content
    }
    // 提交AJAX请求
    $.ajax({
        url: "article",
        type: "POST",
        dataType: "text",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(comment),
        success: function (data) {
            parent.layui.admin.events.closeThisTabs();
        },
        error: function (data) {
            console.log("报错了，未知原因");
        }
    })

    // 清空正文
    localStorage.removeItem("vditorarticle"+articleId);
    // 清空摘要
    localStorage.removeItem("vditorabstract"+articleId);


}
