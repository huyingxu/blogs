(function( $ ){
    // 当domReady的时候开始初始化
    $(function() {
		
		var filePicker = $('#files_img'),
		
		// 优化retina, 在retina下这个值是2
		ratio = window.devicePixelRatio || 1,
		
		// 缩略图大小
		thumbnailWidth = 40 * ratio,
		thumbnailHeight = 40 * ratio,
		
		// 所有文件的进度信息，key为file id
		percentages = {},
		// 判断浏览器是否支持图片的base64
		isSupportBase64 = ( function() {
			var data = new Image();
			var support = true;
			data.onload = data.onerror = function() {
				if( this.width != 1 || this.height != 1 ) {
					support = false;
				}
			}
			data.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
			return support;
		} )(),
		// 检测是否已经安装flash，检测flash的版本
		flashVersion = ( function() {
			var version;
			try {
				version = navigator.plugins[ 'Shockwave Flash' ];
				version = version.description;
			} catch ( ex ) {
				try {
					version = new ActiveXObject('ShockwaveFlash.ShockwaveFlash')
					.GetVariable('$version');
				} catch ( ex2 ) {
					version = '0.0';
				}
			}
			version = version.match( /\d+/g );
			return parseFloat( version[ 0 ] + '.' + version[ 1 ], 10 );
		} )(),
		
		supportTransition = (function(){
			var s = document.createElement('p').style,
			r = 'transition' in s ||
			'WebkitTransition' in s ||
			'MozTransition' in s ||
			'msTransition' in s ||
			'OTransition' in s;
			s = null;
			return r;
		})(),
		// WebUploader实例
		uploader;
    	//对于uploader的创建，最好等dom元素也就是下面的div创建好之后再创建，因为里面有用到选择文件按钮，
    	//不然会创建报错，这是很容易忽视的地方，故这里放到$(function(){}来进行创建*/
         uploader = WebUploader.create({
             // swf文件路径
             swf: path+'static/webuploader/dist/Uploader.swf',
             // 文件接收服务端。
             server: path+'/uploadFile',
             // 选择文件的按钮。可选。
             // 内部根据当前运行是创建，可能是input元素，也可能是flash.
             pick:{
                     id:filePicker
             },
             //文件上传类型
     		 accept:{
     			 
    		 },
    		 thumb:{
    			width: 40,
    			height: 40,
    			// 是否允许放大，如果想要生成小图的时候不失真，此选项应该设置为false.
    			allowMagnify: true,
    			// 是否允许裁剪。
    			crop: true
    		 },
             // 上传并发数。允许同时最大上传进程数[默认值：3]   即上传文件数
             threads: 3,
             // 自动上传修改为手动上传
             auto: false,
             //是否要分片处理大文件上传。
             chunked: false,
             // 如果要分片，分多大一片？ 默认大小为5M.
             chunkSize: 5 * 1024 * 1024,
             // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
             resize: false,
             //文件上传数量
             fileNumLimit: 10,
 			 fileSizeLimit: 200 * 1024 * 1024,    // 200 M
 			 fileSingleSizeLimit: 50 * 1024 * 1024    // 50 M
         });
		 uploader.on('ready', function() {
			window.uploader = uploader;
		 });
         //当有文件添加进来的时候触发
         uploader.on('fileQueued', function (file) {
        	 uploader.upload();
         });
         // 当文件被移除队列后触发
         uploader.on('fileDequeued', function (file) {
        	var $li = $('#'+file.id);
 			$li.remove();
         });
         // 文件上传成功处理。
         uploader.on('uploadSuccess', function (file, response) {
        	 
         });
         // 文件上传失败处理。
         uploader.on('uploadError', function (file, reason) {
        	 alert( 'Error: ' + file + '上传错误,'+  reason );
         });
     });
})( jQuery );
    
    