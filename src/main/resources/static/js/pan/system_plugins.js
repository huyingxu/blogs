$(function(){
	$.fn.extend({
		//分页插件
	    loadFY:function(json,cl,url,page){
	    	var _this=this;
	    	var li='';
			var prevDis='';
			if(json.obj.totalPage>1){
				if(json.obj.pageNum==1){
					prevDis='disabled';
				}
				li+='<li class="prev '+prevDis+'" id="onePage"><a href="javascript:void(0)" onclick="">首页</a></li>';
				li+='<li class="prev '+prevDis+'" id="prevPage"><a href="javascript:void(0)" onclick="">';
				li+='<i class="icon-double-angle-left"></i></a></li>';
				for(var i=1;i<=json.obj.totalPage;i++){
					var active='';
					if(i==json.obj.pageNum){
						active='active';
					}
					li+='<li class="'+active+' thisPage" name="'+i+'"><a href="javascript:void(0)">'+i+'</a></li>';
				}
				var nextDis='';
				if(json.obj.totalPage==json.obj.pageNum){
					nextDis='disabled';
				}
				li+='<li class="next '+nextDis+'" id="nextPage"><a href="javascript:void(0)" onclick="">';
				li+='<i class="icon-double-angle-right"></i></a></li>';
				li+='<li class="prev '+nextDis+'" id="endPage"><a href="javascript:void(0)" onclick="">末页</a></li>';
			}
			_this.siblings(".row").find("#fy>ul").find("li").remove();
			_this.siblings(".row").find("#fy>ul").append(li);
			
			$("#onePage").click(function(){
				page.pageNum=1;
				_this.loadTable(cl, url, page, true);
			});
			$("#endPage").click(function(){
				page.pageNum=json.obj.totalPage;
				_this.loadTable(cl, url, page, true);
			});
			$(".thisPage").click(function(){
				var i=$(this).attr("name");
				page.pageNum=i;
				_this.loadTable(cl, url, page, true);
			});
			$("#prevPage").click(function(){
				if(page.pageNum>1){
					page.pageNum=page.pageNum-1;
					_this.loadTable(cl, url, page, true);
				}
			});
			$("#nextPage").click(function(){
				if(json.obj.totalPage>page.pageNum){
					page.pageNum=page.pageNum+1;
					_this.loadTable(cl, url, page, true);
				}
			});
	    },
		/**
		 * 表格插件
		 * @cl 每列所要显示的数据
		 * @url 查询数据所在路径
		 * @page 页码，每页显示的数据
		 * @bool 是否分页
		 */
	    loadTable:function(cl,url,page,boolFY){
	    	var _this=this;
	    	$.ajax({
				url:url,
				type:"post",
				data:page,
				success:function(data){
					var json=$.parseJSON(data);
					var res=json.obj.results;
					var tr="";
					if(res==null || res=="")return;
					for(var i=0;i<res.length;i++){
				    	tr+='<tr>';
				    	for(var j=0;j<cl.length;j++){
				    		var tab_html="";
				    		if(typeof cl[j].cl=="function" && cl[j].cl!=null){
				    			tab_html=cl[j].cl(j,res[i][cl[j].name]);
				    		}
				    		if(tab_html!=""){
				    			tr+='<td class="center">'+tab_html+'</td>';
				    		}else{
				    			tr+='<td class="center">'+res[i][cl[j].name]+'</td>';
				    		}
				    	}
				    	tr+='</tr>';
					}
					_this.children("tbody").eq(0).find("tr").remove();
					_this.children("tbody").eq(0).append(tr);
					if(boolFY){
						_this.loadFY(json,cl,url,page);
					}
				}
		    });
	    },
	    /**
	     * 下拉框加载插件
	     * @uid value值
	     * @name 显示的数据
	     */
		loadCombox:function(uid,name){
			this.find("input[on_type='comBox']").each(function(index,_this){
				var id=$(_this).attr("id");
				var value=$(_this).attr("value");
				var url=$(_this).attr("loadUrl");
				var select='<select id="'+id+'_select" name="'+uid+'" class="col-xs-12 col-sm-6"></select>';
				$(_this).after(select);
				$.ajax({
					url:url,
					type:"post",
					data:{},
					success:function(data){
						var json=$.parseJSON(data);
						var res=json.obj;
						var option='';
						for(var i=0;i<res.length;i++){
							if(value=='' || value==null){
								option+='<option value="'+res[i][uid]+'">'+res[i][name]+'</option>';
							}else{
								var selected='';
								if(value==res[i][id]){
									selected=' selected="selected"';
								}
								option+='<option value="'+res[i][uid]+'"'+selected+'>'+res[i][name]+'</option>';
							}
						}
						$("#"+id+"_select").append(option);
					}
				});
			});
		},
		/**
		 * 修改插件
		 * col显示数据：type:是否影藏,name:前缀,id:数据名,tyid:加载loadCombox,cl:回调函数
		 * url:提交数据地址
		 * obj:提交参数
		 */
		loadUP:function(col,url,obj){
			var _this=this;
			$.ajax({
				url:url,
				type:"post",
				data:obj,
				success:function(data){
					var json=$.parseJSON(data);
					var res=json.obj;
					var input='';
			    	for(var j=0;j<col.length;j++){
			    		if(col[j].type){
			    			input+='<input type="hidden" value="'+res[col[j].id]+'" name="'+col[j].id+'"/>';
			    		}
			    		if(!col[j].type){
				    		input+='<div class="space-4"></div><div class="form-group row">';
				    		input+='<label class="col-sm-3 control-label no-padding-right" for="form-field-1">';
				    		input+=''+col[j].name+'</label><div class="col-sm-9"'+col[j].tyid+'>';
				    		if(typeof col[j].cl=="function" && col[j].cl!=null){
				    			input+=col[j].cl(j,res[col[j].id]);
				    		}else{
				    			input+='<input type="text" value="'+res[col[j].id]+'" name="'+col[j].id+'" class="col-xs-12 col-sm-6" />';
				    		}
				    		input+='</div></div>';
			    		}
			    	}
			    	input+='<div class="space-4"></div><div class="clearfix form-actions"><div class="col-md-offset-3 col-md-9">';
			    	input+='<button class="btn btn-info" id="btnSub" onclick="subUp()" type="button"><i class="icon-ok bigger-110"></i>';
					input+='Submit</button>&nbsp; &nbsp; &nbsp;<button class="btn" type="reset">';
					input+='<i class="icon-undo bigger-110"></i>Reset</button></div></div>';
			    	_this.children().remove();
			    	_this.append(input);
					$("#userType").loadCombox("usertypeid","typename");
					$("#Indus").loadCombox("industryid","industry");
					$("#clo").loadCombox("columnid","columnname");
				}
			});
		},
		//影藏窗口插件
		closeModel:function(){
			this.css('display','none');
		},
		//加载表单中的数据并转为JSON格式插件
		jsonFromValue:function(){
			var dataAry=new Array();
			this.find(":input").each(function(index,_this){
				var inpName=$(_this).attr("name");
				var val;
				if ($(_this).attr('type') == 'checkbox') {
					if($(_this).prop('checked')){
						val = $(_this).val();
					}else{
						inpName="";
					}
				} else if ($(_this).attr('type') == 'radio') {
					if($(_this).prop('checked')){
						val = $(_this).val();
					}else{
						inpName="";
					}
				} else{
					val = $(_this).val();
				}
				if(inpName!="" & inpName!=null & val != null & val != ""){
					dataAry.push(inpName+":'"+val+"'");
				}
			});
			var jsonObj="{"+dataAry.join(",")+"}";
			var jsonForm=eval("("+jsonObj+")");
			return jsonForm;
		},
		//加载树列父级菜单
		loadTreeData:function(url,id,name){
			var tree = new Array();
			$.ajax({
				url:url,
				type:"post",
				data:{},
				async:false,
				success:function(data){
					var json=$.parseJSON(data);
					var res=json.obj;
					for(var i=0;i<res.length;i++){
						tree.push(res[i][id]+":{name:'<a onclick=\"tree(this);\" cloid=\""+res[i][id]+"\">"+res[i][name]+"</a>',type:'folder'}");
					}
				}
			});
			var jsonObj="{"+tree.join(",")+"}";
			var jsonTree=eval("("+jsonObj+")");
			return jsonTree;
		},
		//加载树列的子级菜单 tree:父级菜单
		loadTreeDataChildren:function(url,id,name,tree){
			for(var key in tree){
				var jsAry=new Array();
				$.ajax({
					url:url,
					type:"post",
					data:{id:key},
					async:false,
					success:function(data){
						var json=$.parseJSON(data);
						var res=json.obj;
						for(var j=0;j<res.length;j++){
							jsAry.push("'"+res[j][id]+"':{name:'<i class=\"icon-book blue\"></i>"+res[j][name]+"', type: 'item'}");
						}
					}
				});
				var jsonObj="{children:{"+jsAry.join(",")+"}}";
				var jsonTree=eval("("+jsonObj+")");
				tree[key].additionalParameters=jsonTree;
			}
			return tree;
		},
		//前端加载新闻
		loadNewsByClo:function(url,data){
			var _this=this;
			$.ajax({
				url:url,
				type:"post",
				data:data,
				success:function(data){
					var json=$.parseJSON(data);
					var res=json.obj.results;
					var div='';
					if(res==null){_this.children().remove();$.DialogByZ.Autofade({Content:"该栏目没有数据!!!"});return;}
					for(var i=0;i<res.length;i++){//'+path+'/'+res[i].storagepath+'
						div+='<div class="row news" onclick="newsDetails(this)" value="'+res[i].newsid+'"><div class="col-xs-4"><img src="/SpringDemo/upload/file/2018113062b3000778314151840e86d5a98bcc54.png">';
						div+='</div><div class="col-xs-8"><div class="row font-1"><div class="col-xs-12">标题:'+res[i].title+'</div>';
						var rong='';
						if(res[i].newscontent.length>=150){
							rong=''+res[i].newscontent.substr(0,150)+'...';
							div+='</div><div class="row font-2 content"><div class="col-xs-12">'+rong+'</div>';
						}else{
							div+='</div><div class="row font-2 content"><div class="col-xs-12">'+res[i].newscontent+'</div>';
						}
						var time=new Date(res[i].releasetime).Format("yyyy-MM-dd HH:mm");
						div+='</div><div class="row font-3"><div class="col-xs-6 time">'+time+'</div>';
						div+='<div class="col-xs-6 source">来源:和讯网</div></div></div></div>';
					}
					_this.children().remove();
					_this.append(div);
				}
			});
		},
		//前端加载titleBuff
		titleBuff:function(url,data){
			var _this=this;
			$.ajax({
				url:url,
				type:"post",
				data:data,
				success:function(data){
					var json=$.parseJSON(data);
					var res=json.obj.results;
					var clo='<div class="col-xs-1"></div>';
					var active='';
					for(var j=0;j<res.length;j++){
						if(j==0){
							active='title-buff-active';
						}else{
							active='';
						}
						clo+='<div class="col-xs-2 title-buff-t '+active+'" value="'+res[j].columnid+'" onclick="titleBuff(this)">'+res[j].columnname+'</div>';
					}
					clo+='<div class="col-xs-1"></div>';
					_this.children().remove();
					_this.append(clo);
				}
			});
		},
		//前端加载轮播
		loadCarousel:function(url,data){
			var _this=this;
			$.ajax({
				url:url,
				type:"post",
				data:data,
				success:function(data){
					var json=$.parseJSON(data);
					var res=json.obj.results;
					var divCarousel='<div id="myCarousel" class="carousel slide myCarousel">';
				    divCarousel+='<div class="carousel-inner">';
				    var divIndicators='';
					for(var i=0;i<res.length;i++){
				    	var active='';
					    if(i==0){
					    	active='active';
					    }else{
					    	active='';
					    }
				        divIndicators+='<li data-target="#myCarousel" data-slide-to="'+i+'" class="'+active+'"></li>';
					    divCarousel+='<div class="item '+active+'">';
					    divCarousel+='<img src="'+path+'/'+res[i].storagepath+'" alt="First slide"><div class="carousel-caption"></div></div>';
				    }
					divCarousel+='</div><ol class="carousel-indicators">'+divIndicators;
				    divCarousel+='</ol></div>';
					_this.children().remove();
				    _this.append(divCarousel);
				}
			});
		},
		//加载a标签传递的参数
		parseUrl:function(){
			var url=location.href;
			var i=url.indexOf('?');
			if(i==-1)return;
			var querystr=url.substr(i+1);
			var arr1=querystr.split('&');
			var arr2=new Object();
			for  (i in arr1){
			    var ta=arr1[i].split('=');
			    arr2[ta[0]]=ta[1];
			}
			return arr2;
		}
	});
});