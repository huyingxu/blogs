--  修改 表名
alter table t_message rename to t_cloud_message;

-- ----------------------------
-- View structure for cloud_message
-- ----------------------------
DROP VIEW IF EXISTS `view_cloud_message`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_cloud_message` AS select `tm`.`id` AS `id`,`tm`.`template_id` AS `template_id`,`tca`.`accesskey_id` AS `accesskey_id`,`tca`.`accesskey_secret` AS `accesskey_secret`,`tm`.`app_id` AS `app_id`,`tm`.`sign` AS `sign`,`tm`.`template_content` AS `template_content`,(case when (`tm`.`status` = '2') then 'true' else 'false' end) AS `def` from (`t_cloud_message` `tm` left join `t_cloud_accesskey` `tca` on((`tca`.`id` = `tm`.`key_id`))) where (`tca`.`status` = 3);
