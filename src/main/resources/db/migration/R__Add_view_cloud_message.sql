-- 创建短信模板视图

CREATE ALGORITHM = UNDEFINED DEFINER = `root` @`%` SQL SECURITY DEFINER VIEW `view_cloud_message` AS SELECT
`tm`.`id` AS `id`,
`tm`.`template_id` AS `template_id`,
`tca`.`accesskey_id` AS `accesskey_id`,
`tca`.`accesskey_secret` AS `accesskey_secret`,
`tm`.`app_id` AS `app_id`,
`tm`.`sign` AS `sign`,
`tm`.`template_content` AS `template_content`,
`tm`.`region` AS `region`,
`tm`.`useBy` AS `useBy`,
( CASE WHEN ( `tm`.`status` = '2' ) THEN 'true' ELSE 'false' END ) AS `def`
FROM
	( `t_cloud_message` `tm` LEFT JOIN `t_cloud_accesskey` `tca` ON ( ( `tca`.`id` = `tm`.`key_id` ) ) )
WHERE
	( `tca`.`status` = 3 );