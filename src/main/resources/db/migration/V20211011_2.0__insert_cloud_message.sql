-- 添加短信模板(参考)

INSERT INTO `blogs`.`t_cloud_message`(`id`, `template_id`, `sign`, `key_id`, `app_id`, `region`, `status`, `template_content`, `useBy`) VALUES (1, '527862', '默认发送模板', 3, '1400308545', 'ap-beijing', '1', '您的验证码为：{1}，该验证码{2}分钟内有效，请勿泄漏于他人！', 'default');
INSERT INTO `blogs`.`t_cloud_message`(`id`, `template_id`, `sign`, `key_id`, `app_id`, `region`, `status`, `template_content`, `useBy`) VALUES (2, '526681', '登陆', 3, '1400308545', 'ap-beijing', '1', '{1}为您的登录验证码，请于{2}分钟内填写，如非本人操作，请忽略本短信。', 'login');
INSERT INTO `blogs`.`t_cloud_message`(`id`, `template_id`, `sign`, `key_id`, `app_id`, `region`, `status`, `template_content`, `useBy`) VALUES (3, '526636', '身份验证\n', 3, '1400308545', 'ap-beijing', '2', '您的验证码为：{1}，该验证码5分钟内有效，请勿泄漏于他人！', 'code');
INSERT INTO `blogs`.`t_cloud_message`(`id`, `template_id`, `sign`, `key_id`, `app_id`, `region`, `status`, `template_content`, `useBy`) VALUES (6, '1152126', '问候语', 3, '1400308545', 'ap-beijing', '1', '亲爱的{1} 早安！今天是{2}年{3}月{4}日 上午{5}。今天{6}区{7}，最高温度 {8}°C 最低温度{9}°C 。', 'weather');

-- 添加短信模板变量数据(参考)

INSERT INTO `blogs`.`t_cloud_message_var`(`id`, `var_name`, `template_id`, `var_sort`) VALUES (1, 'year', 6, 1);
INSERT INTO `blogs`.`t_cloud_message_var`(`id`, `var_name`, `template_id`, `var_sort`) VALUES (2, 'month', 6, 2);
INSERT INTO `blogs`.`t_cloud_message_var`(`id`, `var_name`, `template_id`, `var_sort`) VALUES (3, 'day', 6, 3);
INSERT INTO `blogs`.`t_cloud_message_var`(`id`, `var_name`, `template_id`, `var_sort`) VALUES (8, 'date', 6, 4);
INSERT INTO `blogs`.`t_cloud_message_var`(`id`, `var_name`, `template_id`, `var_sort`) VALUES (6, 'city', 6, 5);
INSERT INTO `blogs`.`t_cloud_message_var`(`id`, `var_name`, `template_id`, `var_sort`) VALUES (7, 'wea', 6, 6);
INSERT INTO `blogs`.`t_cloud_message_var`(`id`, `var_name`, `template_id`, `var_sort`) VALUES (4, 'tem1', 6, 7);
INSERT INTO `blogs`.`t_cloud_message_var`(`id`, `var_name`, `template_id`, `var_sort`) VALUES (5, 'tem2', 6, 8);