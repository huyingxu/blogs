--  插入地域基础数据

-- ----------------------------
-- Records of t_cloud_address
-- ----------------------------

INSERT INTO `t_cloud_address` VALUES (1, 'oss-cn-hangzhou', 'oss-cn-hangzhou.aliyuncs.com', '华东1（杭州）');
INSERT INTO `t_cloud_address` VALUES (2, 'oss-cn-shanghai', 'oss-cn-shanghai.aliyuncs.com', '华东2（上海）');
INSERT INTO `t_cloud_address` VALUES (3, 'oss-cn-qingdao', 'oss-cn-qingdao.aliyuncs.com', '华北1（青岛）');
INSERT INTO `t_cloud_address` VALUES (4, 'oss-cn-beijing', 'oss-cn-beijing.aliyuncs.com', '华北2（北京）');
INSERT INTO `t_cloud_address` VALUES (5, 'oss-cn-zhangjiakou', 'oss-cn-zhangjiakou.aliyuncs.com', '华北3（张家口）');
INSERT INTO `t_cloud_address` VALUES (6, 'oss-cn-huhehaote', 'oss-cn-huhehaote.aliyuncs.com', '华北5（呼和浩特）');
INSERT INTO `t_cloud_address` VALUES (7, 'oss-cn-shenzhen', 'oss-cn-shenzhen.aliyuncs.com', '华南1（深圳）');
INSERT INTO `t_cloud_address` VALUES (8, 'oss-cn-chengdu', 'oss-cn-chengdu.aliyuncs.com', '西南1（成都）');
INSERT INTO `t_cloud_address` VALUES (9, 'oss-cn-hongkong', 'oss-cn-hongkong.aliyuncs.com', '中国（香港）');
INSERT INTO `t_cloud_address` VALUES (10, 'oss-ap-southeast-1', 'oss-ap-southeast-1.aliyuncs.com', '新加坡');
INSERT INTO `t_cloud_address` VALUES (11, 'oss-ap-southeast-2', 'oss-ap-southeast-2.aliyuncs.com', '澳大利亚（悉尼）');
INSERT INTO `t_cloud_address` VALUES (12, 'oss-ap-southeast-3', 'oss-ap-southeast-3.aliyuncs.com', '马来西亚（吉隆坡）');
INSERT INTO `t_cloud_address` VALUES (13, 'oss-ap-southeast-5', 'oss-ap-southeast-5.aliyuncs.com', '印度尼西亚（雅加达）');
INSERT INTO `t_cloud_address` VALUES (14, 'oss-ap-northeast-1', 'oss-ap-northeast-1.aliyuncs.com', '日本（东京）');
INSERT INTO `t_cloud_address` VALUES (15, 'oss-ap-south-1', 'oss-ap-south-1.aliyuncs.com', '印度（孟买）');
INSERT INTO `t_cloud_address` VALUES (16, 'oss-eu-central-1', 'oss-eu-central-1.aliyuncs.com', '德国（法兰克福）');
INSERT INTO `t_cloud_address` VALUES (17, 'oss-eu-west-1', 'oss-eu-west-1.aliyuncs.com', '英国（伦敦）');
INSERT INTO `t_cloud_address` VALUES (18, 'oss-us-west-1', 'oss-us-west-1.aliyuncs.com', '美国（硅谷）');
INSERT INTO `t_cloud_address` VALUES (19, 'oss-us-east-1', 'oss-us-east-1.aliyuncs.com', '美国（弗吉尼亚）');
INSERT INTO `t_cloud_address` VALUES (20, 'oss-me-east-1', 'oss-me-east-1.aliyuncs.com', '阿联酋（迪拜）');

