
-- 添加站点信息

DROP TABLE IF EXISTS `t_site_info`;
CREATE TABLE `t_site_info` (
                             `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
                             `title` varchar(255) NOT NULL COMMENT '标题',
                             `keyword` varchar(255) DEFAULT NULL COMMENT '关键字',
                             `describe` varchar(255) DEFAULT NULL COMMENT '描述',
                             `qq` varchar(255) DEFAULT NULL COMMENT 'QQ',
                             `tel` varchar(255) DEFAULT NULL COMMENT '电话',
                             `email` varchar(255) DEFAULT NULL COMMENT '邮件',
                             `address` varchar(255) DEFAULT NULL COMMENT '地址',
                             `scopyright` varchar(255) DEFAULT NULL COMMENT '底部信息',
                             `create_by` datetime DEFAULT NULL COMMENT '创建时间',
                             `modified_by` datetime DEFAULT NULL COMMENT '修改时间',
                             `create_user` varchar(255) DEFAULT NULL COMMENT '创建者/修改者',
                             `logo_url` varchar(255) DEFAULT NULL COMMENT 'logo',
                             `icon_url` varchar(255) DEFAULT NULL COMMENT '图标',
                             `rate` varchar(255) DEFAULT NULL COMMENT '访问量',
                             PRIMARY KEY (`id`),
                             KEY `t_site_info_ibfk_1` (`create_user`),
                             CONSTRAINT `t_site_info_ibfk_1` FOREIGN KEY (`create_user`) REFERENCES `t_user` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
