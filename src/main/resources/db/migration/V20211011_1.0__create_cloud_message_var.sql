-- 短信模板变量

CREATE TABLE `t_cloud_message_var` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `var_name` varchar(255) DEFAULT NULL,
  `template_id` int(255) NOT NULL COMMENT '短信模板',
  `var_sort` int(255) DEFAULT NULL COMMENT '短信模板变量排序',
  PRIMARY KEY (`id`),
  KEY `t_cloud_sms_var_ibfk_1` (`template_id`),
  CONSTRAINT `t_cloud_message_var_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `t_cloud_message` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;