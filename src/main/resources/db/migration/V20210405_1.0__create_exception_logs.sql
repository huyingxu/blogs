--  创建异常日志记录表

-- ----------------------------
-- Table structure for t_exception_logs
-- ----------------------------
DROP TABLE IF EXISTS `t_exception_logs`;
CREATE TABLE `t_exception_logs` (
    `uuid` varchar(255) NOT NULL DEFAULT '' COMMENT 'UUID',
    `error_code` int(11) DEFAULT NULL COMMENT '错误代码',
    `error_title` text COMMENT '错误标题',
    `error_message` text COMMENT '状态信息',
    `error_details` text COMMENT '错误信息',
    `is_read` int(11) DEFAULT '0' COMMENT '是否已读 0 未读 1已读',
    `visit_ip` varchar(255) DEFAULT NULL COMMENT '访问地址',
    `ip_statis` int(11) DEFAULT NULL COMMENT '同IP异常统计',
    `visit_user` varchar(255) DEFAULT NULL COMMENT '用户名',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;