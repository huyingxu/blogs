
-- 添加 is_used 字段
alter table t_article_info add column is_used int(11) default 0 comment "状态      0 待发布 1 发布 2 删除";

-- 将已有文章改为发布状态
update t_article_info set is_used = 1;