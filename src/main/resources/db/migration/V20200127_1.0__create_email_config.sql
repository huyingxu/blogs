-- ----------------------------
-- Table structure for t_email_config
-- ----------------------------
DROP TABLE IF EXISTS `t_email_config`;
CREATE TABLE `t_email_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '邮箱账号',
  `password` varchar(255) DEFAULT NULL COMMENT '授权码',
  `host` varchar(255) DEFAULT NULL COMMENT '服务器地址',
  `port` int(255) DEFAULT '25' COMMENT '端口',
  `status` varchar(255) DEFAULT '1' COMMENT '状态 0 禁用 1 启用 2 默认',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;