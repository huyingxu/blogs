CREATE TABLE `t_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` varchar(255) DEFAULT NULL COMMENT '模板id',
  `sign` varchar(255) DEFAULT NULL COMMENT '签名',
  `key_id` int(11) DEFAULT NULL COMMENT '秘钥id',
  `app_id` int(11) DEFAULT NULL COMMENT '应用id',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `t_message_ibfk_1` (`key_id`),
  KEY `t_message_ibfk_2` (`app_id`),
  CONSTRAINT `t_message_ibfk_1` FOREIGN KEY (`key_id`) REFERENCES `t_cloud_accesskey` (`id`),
  CONSTRAINT `t_message_ibfk_2` FOREIGN KEY (`app_id`) REFERENCES `t_cloud_accesskey` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;