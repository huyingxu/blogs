-- ----------------------------
-- Table structure for t_cloud_accesskey
-- ----------------------------
CREATE TABLE `t_cloud_accesskey` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `accesskey_id` varchar(255) DEFAULT NULL COMMENT '访问秘钥id',
  `accesskey_secret` varchar(255) DEFAULT NULL COMMENT '访问秘钥',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_cloud_address
-- ----------------------------
CREATE TABLE `t_cloud_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regionLocation` varchar(255) DEFAULT NULL,
  `endpoint` varchar(255) DEFAULT NULL,
  `regionNames` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for t_cloud_bucket
-- ----------------------------
CREATE TABLE `t_cloud_bucket` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `bucket_name` varchar(255) NOT NULL COMMENT '仓库名字',
  `addr_id` int(11) DEFAULT NULL COMMENT '地区',
  `key_id` int(11) DEFAULT NULL COMMENT '秘钥',
  `status` varchar(255) DEFAULT '1' COMMENT '状态 0 已删除 1 正常 2  默认仓库',
  PRIMARY KEY (`id`),
  UNIQUE KEY `bucket_name` (`bucket_name`),
  KEY `t_cloud_bucket_ibfk_1` (`addr_id`),
  KEY `t_cloud_bucket_ibfk_2` (`key_id`),
  CONSTRAINT `t_cloud_bucket_ibfk_1` FOREIGN KEY (`addr_id`) REFERENCES `t_cloud_address` (`id`),
  CONSTRAINT `t_cloud_bucket_ibfk_2` FOREIGN KEY (`key_id`) REFERENCES `t_cloud_accesskey` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;