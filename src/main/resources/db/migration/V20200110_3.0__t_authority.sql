-- 权限表
CREATE TABLE `t_authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authority_name` varchar(140) DEFAULT NULL,
  `cname` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- 用户和权限关联表

CREATE TABLE `t_user_authority` (
  `username` varchar(35) NOT NULL,
  `authority_id` bigint(20) DEFAULT NULL,
  KEY `user_authority_ibfk_1` (`authority_id`),
  KEY `t_user_authority_ibfk_2` (`username`),
  CONSTRAINT `t_user_authority_ibfk_1` FOREIGN KEY (`authority_id`) REFERENCES `t_authority` (`id`),
  CONSTRAINT `t_user_authority_ibfk_2` FOREIGN KEY (`username`) REFERENCES `t_user` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
