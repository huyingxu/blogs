-- 存放项目所有云API
CREATE TABLE `t_cloud_link` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `info` varchar(255) DEFAULT NULL,
                                `link` varchar(255) DEFAULT NULL,
                                `type` varchar(255) DEFAULT NULL,
                                `useBy` varchar(255) DEFAULT NULL,
                                `status` varchar(255) DEFAULT '1',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 获取whois信息
INSERT INTO `blogs`.`t_cloud_link`(`id`, `info`, `link`, `useBy`, `type`, `status`) VALUES (1, '获取whois信息', 'http://whois.pconline.com.cn/ip.jsp?ip=', 'dip', 'api', '1');
-- 获取天气信息
INSERT INTO `blogs`.`t_cloud_link`(`id`, `info`, `link`, `useBy`, `type`, `status`) VALUES (2, '获取天气信息', 'https://tianqiapi.com/api', 'weather', 'api', '1');