FROM slera/jdk:8

ENV workdir /app/

ENV jar blogs-null.jar
COPY target/${jar} ${workdir}
WORKDIR ${workdir}

HEALTHCHECK --interval=60s --timeout=3s CMD /bin/bash /app/health.sh
CMD ["sh","-ec","/app/start.sh"]
